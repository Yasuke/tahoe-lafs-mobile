# Mobile Architecture

**Contacts**: Jean-Paul

**Date**: 2023-01-09

## Context

Initially the PrivateStorage mobile application will co-exist with desktop usage of Gridsync and an existing Tahoe-LAFS storage server grid.
Eventually it will likely co-exist with many more applications.
It is therefore desirable for the implementation to share certain properties with these systems.
For example, it will use Tahoe-LAFS as the storage layer because of these two existing components.

## Language/Runtime Choice

The obvious language choice,
due to its use in Tahoe-LAFS and Gridsync,
is Python.
However, Python and Android platform limitations [make this infeasible](bizops#250).

The next best choice is a language that is suitable for at least iOS in addition to Android development.
Ideally it would also be suitable for Linux and Windows desktop development.
This will mitigate against the risk of having to introduce a *third* language in the foreseeable future.

Other considerations for language selection include:

* Free software
* Supports development of low-defect applications
* Performs well at runtime for cryptographic workloads

Programming languages and runtimes are extremely complex systems and it is not practical to perform an exhaustive comparison of all options available to us.
What follows is an extremely brief comparison of what seem to be the few most obvious practical options.

### Candidates

#### Java / Kotlin

Java and Kotlin are the languages of choice for Android development.
However, they offer little in the way of support for iOS development.
Java and Kotlin are both procedural languages which don't strongly support the creation of software that is easy to reason about due to statefulness and side-effects.

Java is maintained by Oracle.
Oracle has traditionally opposed many free software ideas.
Kotlin is technically maintained by the Kotlin Foundation but in practice owes its Android support to Google.
Google has a complex relationship with free software but has frequently demonstrated it is willing to kill a product or free software community if doing so may benefit Google in some way.

#### Flutter / Dart

Flutter is a development toolkit for all major desktop and mobile platforms
(including one that isn't even real yet, Google Fuchsia)
as well as the web.
Flutter is an increasingly popular runtime for Android and other development.
Flutter relies on the Dart language.
Dart is a procedural language with the consequences described above.

Flutter and Dart are both created solely by Google.
They are both free software.

#### Haskell

Haskell is a general purpose programming language with support for all major platforms.
Haskell is a pure functional language which encourages the isolation of stateful operations and makes side-effects a first-class part of the type system.
These properties help make reasoning about software written in Haskell easier.
Haskell's type system also makes software maintenance over the long term easier by allowing the toolchain to provide more help.

##### Haskell / Obelisk

Obelisk is a Haskell toolkit for building Android, iOS, Linux desktop, and web applications.
An Obelisk application can be written in Haskell alone
(relying on CSS and DOM for visual customization).

This may be appealing because it limits the complexity to that of Haskell which will make the project easier to understand, develop, and maintain.
One downside of this approach is that Obelisk development has not been focused on Android platform integration.
As a result, some tasks involving Android platform integration may be difficult.
Another downside is the relatively small number of Obelisk users
(resulting in a small community of developers from which to draw help).
A mitigating factor for this downside is the existence of ObsidianSystems.
ObsidianSystems specializes in rapid application development with Obelisk.
ObsidianSystems has indicated their availability to support our efforts.

##### Haskell / Flutter

It is possible to write complex application logic in Haskell and then expose it to Dart via an FFI.
The relatively complex Tahoe-LAFS logic could be implemented in Haskell
(a language well-suited for writing complex logic).
The Android integration and GUI could be implemented with Flutter in Dart.
Dart would then invoke the Tahoe-LAFS APIs implemented in Haskell via FFI.

This may be appealing because Flutter is likely to have a far more well-trod path for Android platform integration tasks.
The downside of this approach is that it involves all of the complexity of Haskell and all of the complexity of Flutter.

### Decision

"Haskell / Obelisk" is the most appealing of the options considered here.
It presents the possibility of a single-language stack solution --
with a very expressive, safe, performant language.

However, we have not yet demonstrated that it can perform all of the Android platform integration tasks we expect to require.
Consequently, it would be premature to claim we can decide on it at this time.
Further investigation is required to answer this remaining open question.

Lauri, Chris, meejah, Shae, and Jean-Paul discussed the options on 2023-01-09.
The conclusion is that since as far as we can tell Obelisk can do the bare minimum as required to meet the DRL grant requirements that we will pursue Haskell / Obelisk as the technology stack.
At minimum this will result in the creation of a set of useful Tahoe-LAFS libraries implemented in Haskell.
These will be valuable regardless of all other outcomes of the project.
At minimum this will also result in a barebones functional application to satisfy the requirements of the DRL grant.

We will continue to engage with ObsidianSystems to support this effort.
Shae and Jean-Paul will be primarily responsible for Haskell development.
Chris will support creation of the UI by bringing UI/UX domain expertise to the effort.
Chris and meejah are also both interested in continue to learn more about Haskell and Obelisk and may be involved in the effort in other ways as well.

After the DRL requirements are met we can evaluate how the Obelisk-based effort has gone.
With the experience gained in that effort we can determine whether it makes sense to continue to use Obelisk or switch efforts to a different system for the UI component.

We rule out Kotlin and Java due to lack of interest among the current Private Storage developers, because of their unsuitability for the complex task of implementing Tahoe-LAFS, and due to their relatively limited multi-platform support.

We rule out Flutter (for the time being) due to lack of familiarity among the current core Private Storage developers because we are unsure how much effort we can count on from other Least Authority developers who do have Flutter experience.

## Detail Design

Some material exists elsewhere.
Link out to it.
Otherwise, todo.

* [Lightweight Design](./designs/lightweight-design.md)
