# Tahoe-LAFS Mobile

[![Most recent pipeline status badge](https://gitlab.com/tahoe-lafs/tahoe-lafs-mobile/badges/main/pipeline.svg)](https://gitlab.com/tahoe-lafs/tahoe-lafs-mobile/-/pipelines?scope=all&ref=main)

![Screenshot 1](https://gitlab.com/tahoe-lafs/tahoe-lafs-mobile/-/raw/main/metadata/en-US/images/phoneScreenshots/1.png){width=32%}
![Screenshot 2](https://gitlab.com/tahoe-lafs/tahoe-lafs-mobile/-/raw/main/metadata/en-US/images/phoneScreenshots/2.png){width=32%}
![Screenshot 3](https://gitlab.com/tahoe-lafs/tahoe-lafs-mobile/-/raw/main/metadata/en-US/images/phoneScreenshots/3.png){width=32%}

## Description

Mobile application for accessing files stored using [Tahoe-LAFS](https://tahoe-lafs.org), building on / forking [PrivateStorage Mobile](https://whetstone.private.storage/privatestorage/privatestoragemobile/).

## License

This application is licensed GPL-3.0-only with app-store exception (an additional permission under section 7).
Please see the `LICENSE` file.

## Overall plan

We have [milestones](https://gitlab.com/tahoe-lafs/tahoe-lafs-mobile/-/milestones) and [tickets](https://gitlab.com/tahoe-lafs/tahoe-lafs-mobile/-/issues) for a rough idea of who's working on what, what's completed, and what's blocking.

## Project status

This application is in the early stages of development.
See the issue tracker and the overall plan document for detailed status information.

## Build

Build an Android debug apk and install it to an Android device connected via USB
(with necessary permissions granted):

```sh
cd obelisk
nix-build -A android.frontend -o result-android-frontend
./result-android-frontend/bin/deploy
```

## Release

Build an Android release apk:

```sh
cd obelisk
nix-build -A android.frontend.release -o result-android-release
./result-android-release/bin/deploy
```

## Release

If you want to sign a build with the release key then you need to a signing key.

A new key store containing a signing key can be created with `keytool` like:

```sh
$ keytool \
    -genkey \
        -v \
        -keystore ./android-release.keystore \
        -alias release \
        -keyalg RSA \
        -keysize 2048 \
        -validity 36500 \
        -dname "CN=...;OU=...;O=...;L=...;ST=...;C=..."
```

The store password must be known to the release derivation.
`obelisk/default.nix` expects `obelisk/android-signing-key.nix` to exist to find this information.
See `obelisk/android-signing-key-example.nix` for an example of this file.
Note:

* values in the *dname* must have the following characters backslash-escaped: `,+=<>#;`.
* the validity period must be at least 50 years.
* the tool warns about using a proprietary key store format but this format seems to be required by reflex-platform.

Build an Android release Bundle (aab):

```sh
cd obelisk
nix-build -A android.frontend.bundle -o result-android-bundle
```

Ignore the ``deploy`` script in the Bundle result.
Bundles are not directly installable on devices.

## Run Locally

You can run the Obelisk application locally, without a phone.
To do so:

```sh
cd obelisk
nix-shell
ob run
```

This should eventually say something like ``Frontend running on http://localhost:8008`` and you can then visit that address.
Theoretically, "a Web browser" is what's needed but Obelisk shortcomings currently demand Chrome (or Chromium).
It is advisable to make the browser window "phone-shaped" for a decent experience.
You can use "Developer Tools" to exactly size it to particular phones if you prefer.


## Update Dependencies

### nix-thunk

Some dependencies are managed using [nix-thunk](https://github.com/obsidiansystems/nix-thunk).
These can be found in ``obelisk/dep``.

Nix thunks are essentially references to git repositories.
These can be unpacked to their source in-place when working on the project or packed up into a few small files.

``nix-thunk`` is available in the nix shell provided by ``shell.nix`` at the top of the repository.

Consider an example dependency "botan".
From the top-level of this repo,
to clone a copy of the Botan git repository at the appropriate commit, run:

```sh
nix-thunk unpack obelisk/dep/botan
```

You can work on it in-place and when you are done, pack it up again:

```sh
nix-thunk pack obelisk/dep/botan
```

This requires that changes have been committed and pushed.

Note:
A bug in the current version of Obelisk (link?) causes trouble if certain repos are unpacked.
If you have any trouble running an ``ob`` command (ob run, ob repl, etc.) with a thunk unpacked,
try adding the flag ``--no-interpret obelisk/dep`` and hopefully that will sort it out.


## adding a new one

Use ``nix-thunk create`` to add a new dependency.
For example, to add a thunk for *cowsay*:

```sh
nix-thunk create https://github.com/cowsay-org/cowsay obelisk/dep/cowsay
```

If the dependency is a Haskell library it needs to be added to:

* the Nix Haskell package set
  (see ``obelisk/frontend/frontend.cabal``)
* the relevant section of the project's cabal file
  (eg ``obelisk/frontend/frontend.cabal``)

## updating an existing one

Use ``nix-thunk update`` to modify an existing dependency.
For example, to switch to the release-2023 branch of *cowsay*:

```sh
nix-thunk update --branch release-2023 obelisk/dep/cowsay
```

## `ob run` interactions

``ob run`` does not pick up new dependencies.
Restart it *and* re-enter your ``nix-shell`` so it can pick up the changes.


## the Android build

Test the Android build to make sure the new dependency works there too:

```sh
cd obelisk
nix-build -A android.frontend
```
