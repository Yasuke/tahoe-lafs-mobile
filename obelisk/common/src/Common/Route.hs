{-# LANGUAGE UndecidableInstances #-}
{-# LANGUAGE ConstraintKinds #-}
{-# LANGUAGE EmptyCase #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE GADTs #-}
{-# LANGUAGE KindSignatures #-}
{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RankNTypes #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE TypeFamilies #-}
module Common.Route where

import Prelude hiding (id, (.))
import Control.Category
import Control.Categorical.Bifunctor (second)
import Control.Monad.Except (MonadError)

import Data.Text (Text)
import Data.Functor.Identity

import Obelisk.Route
import Obelisk.Route.TH

data BackendRoute :: * -> * where
  -- | Used to handle unparseable routes.
  BackendRoute_Missing :: BackendRoute ()
  -- You can define any routes that will be handled specially by the backend here.
  -- i.e. These do not serve the frontend, but do something different, such as serving static files.

-- | Identify a file browser location by a pair of the name of a magic-folder
-- and the path segments identifying a subfolder in that magic-folder.
type FolderLocation = (Text, [Text])

-- | Define the frontend routes.  Each constructor is a different view in the
-- app.  backend will serve the frontend.
data FrontendRoute :: * -> * where
  -- | The view that is initially visible in the app.  This should be
  -- something like a "loading" screen that is rapidly, automatically replaced
  -- by something else.
  FrontendRoute_Main :: FrontendRoute ()

  -- | A technology demo page.  This is not part of the user-facing
  -- application proper.  Instead, it's a dumping ground for experiments and
  -- demos.  The payload is [] for the basic page and each time the directory
  -- widget is used to navigate deeper into a directory hierarchy the visited
  -- directory is pushed on the front of the list.
  FrontendRoute_TechDemo :: FrontendRoute [Text]

  -- | Show the list of magic-folders
  FrontendRoute_MagicFolders :: FrontendRoute ()

  -- | Show the contents of some folder within a magic-folder (maybe the
  -- root).  The first element of the list identifies the magic-folder.  The
  -- subsequent elements identify a folder contained by that magic-folder.
  FrontendRoute_FileBrowser :: FrontendRoute FolderLocation

-- | Define the mapping between routes and URL paths & query parameters.  Be
-- careful to avoid defining overlapping routes because these aren't supported
-- (you'll get a runtime error).
fullRouteEncoder
  :: Encoder (Either Text) Identity (R (FullRoute BackendRoute FrontendRoute)) PageName
fullRouteEncoder = mkFullRouteEncoder
  (FullRoute_Backend BackendRoute_Missing :/ ())
  (\case
      BackendRoute_Missing -> PathSegment "missing" $ unitEncoder mempty)
  (\case
      FrontendRoute_Main -> PathEnd $ unitEncoder mempty
      FrontendRoute_TechDemo -> PathSegment "techdemo" pathOnlyEncoder
      FrontendRoute_MagicFolders -> PathSegment "magic-folders" $ unitEncoder mempty
      FrontendRoute_FileBrowser -> PathSegment "browse" fileBrowserEncoder
  )
  where
    fileBrowserEncoder :: (Applicative check, MonadError Text parse) => Encoder check parse (Text, [Text]) PageName
    fileBrowserEncoder = addPathSegmentEncoder . second pathOnlyEncoder

concat <$> mapM deriveRouteComponent
  [ ''BackendRoute
  , ''FrontendRoute
  ]
