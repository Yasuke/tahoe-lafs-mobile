{ system ? builtins.currentSystem
, nix-thunk ? import ./dep/nix-thunk {}
, obelisk ? import ./.obelisk/impl {
    inherit system;
    iosSdkVersion = "16.1";

    # Here we insert our own nixpkgs overlays. Since it's a bit awkward, a
    # future version of Obelisk will make this more straightforward (link?),
    # but for now, the way to do this is by modifying the reflex-platform
    # function to modify its nixpkgsOverlays argument.
    #
    # We *also* take our own pinned reflex-platform here instead of the one
    # from Obelisk (./.obelisk/impl/depl/reflex-platform) because we have a
    # branch that fixes some issues that affect us.  If/when we update Obelisk
    # we should look carefully at this to see if we still need to use our
    # branch, and if so what might be required to keep it working correctly
    # with Obelisk.
    reflex-platform-func = (args: import (nix-thunk.thunkSource ./dep/reflex-platform)
      (args // { nixpkgsOverlays = args.nixpkgsOverlays ++ [(import ./nixpkgs-overlay.nix { inherit nix-thunk; })]; } ) );

    # You must accept the Android Software Development Kit License Agreement at
    # https://developer.android.com/studio/terms in order to build Android apps.
    # Uncomment and set this to `true` to indicate your acceptance:
    config.android_sdk.accept_license = true;

    # In order to use Let's Encrypt for HTTPS deployments you must accept
    # their terms of service at https://letsencrypt.org/repository/.
    # Uncomment and set this to `true` to indicate your acceptance:
    # terms.security.acme.acceptTerms = false;
  }
}:
let
  # See README.md for information about managing ./android.keystore.
  #
  # android-signing-key.nix is *not* part of this repository since it contains
  # the signing-related secrets and must not be public.
  releaseKeyPath = ./android-signing-key.nix;

  p =
    with obelisk;
    project ./. ({ pkgs, ... }: rec {
      android.applicationId = "org.tahoe_lafs.tahoe_lafs_mobile";
      android.displayName = "Tahoe-LAFS Mobile";
      android.resources = ./frontend/android/res;
      android.iconPath = "@mipmap/ic_launcher";

      # Override target SDK and platform to get our stuff into the play
      # store even when Obelisk is a behind.
      # Requires our own patches to reflex-platform (see commit
      # e7610f5c856008647e7df081e446d3b74db6969a and the history of
      # dep/reflex-platform/github.json for the patches we used).
      #android.platformVersions = [ "29" ];
      #android.gradle.minSdkVersion = "27";
      #android.gradle.compileSdkVersion = "29";
      #android.gradle.targetSdkVersion = "33";

      # Version numbers
      #
      # Keep static (don't do anything clever or even just string interpolation
      # using Nix) since F-Droid is parsing this for automatic updates in their
      # store.
      #
      # Must be a monotonically increasing number; defines what it means to
      # "upgrade" the app.
      android.version.code = "2";
      # The version that is displayed to the end user.
      android.version.name = "0.0.2";

      # We have a provider to define.  It is certainly not a service.  But
      # reflex-platform puts services in the right place for providers so ...
      android.services = ''
            <!-- Define a provider that allows us to generate content URIs so we can
                 share our files with other apps (eg, to let a viewer app view some
                 content we downloaded).

                 See https://developer.android.com/training/secure-file-sharing/setup-sharing
            -->
            <provider
                android:name="systems.obsidian.FileProvider2"
                android:authorities="${android.applicationId}.fileprovider"
                android:grantUriPermissions="true"
                android:exported="false">
                <meta-data
                    android:name="android.support.FILE_PROVIDER_PATHS"
                    android:resource="@xml/filepaths" />
            </provider>
      '';

      # We use magic-wormhole which depends on saltine which depends on libsodium.
      # We have to explicitly mention libsodium here to get it included in the
      # apk.
      android.runtimeSharedLibs = nixpkgs: [
        "${nixpkgs.libsodium}/lib/libsodium.so"
      ];

      ios.bundleIdentifier = "systems.obsidian.obelisk.examples.minimal";
      ios.bundleName = "Obelisk Minimal Example";

      # Override some elements of the Haskell package set - get newer versions,
      # add custom libraries that aren't packaged upstream, etc.
      overrides = pkgs.callPackage ./haskell-overrides.nix {
        inherit nix-thunk;
      };

      shellToolOverrides = ghc: super: {
        # For now, it's too much work to keep HLS healthy. Drop it.
        # inherit (ghc) haskell-language-server ghcid;

        # Manage additional dependencies with Nix.
        nix-thunk = nix-thunk.command;

        # A tool for unpacking Android manifest "binary xml" files.
        inherit (pkgs.python3Packages) androguard;

        # This gets us keytool which is helpful for managing the Android
        # signing keys.
        inherit (pkgs) openjdk;

        # Make sure we have our linter around (and be more hermetic).
        inherit (pkgs.haskellPackages) hlint;
      };
    });

  # Get this optics library for lens-like functionality to simplify some
  # overrides we have to do.
  inherit
    (import "${(builtins.fetchTarball https://github.com/masaeedu/nix-optics/archive/028b4e0f721d902aa97bc3535cb3d265bfc53abb.tar.gz)}/utils.nix")
    optics
    fn;

  releaseKey =
    if builtins.pathExists releaseKeyPath
    then import releaseKeyPath
    else null;

  # Create a new derivation like `frontend` but with Gradle instructions to
  # build a release bundle.
  makeAndroidBundle = frontend:
    # Tell gradle to build the aab.
    frontend.override {
      # "assembleDebug" or "assembleRelease" to build a debug or release apk
      # (respectively) or "bundleRelease" to build a release bundle (aab).
      gradleTask = "bundleRelease";
      inherit releaseKey;
    };

  # Create a new derivation like `frontend` but with Gradle instructions to
  # build a release assembly
  makeReleasePackage = frontend:
    frontend.override {
      gradleTask = "assembleRelease";
      inherit releaseKey;
    };

  # Add other Android frontend configurations to the given Obelisk project.
  addOtherPackages =
    with optics fn;
    path
      [ "android" "frontend" ]
      (frontend: (frontend // {
        bundle = makeAndroidBundle frontend;
        release = makeReleasePackage frontend;
      }));


in
(addOtherPackages p) // {
  # Make sure we use the same nixpkgs here as we gave to obelisk /
  # reflex-platform above.
  inherit (obelisk) nixpkgs;
}
