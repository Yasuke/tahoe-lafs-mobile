-- | Implement platform-specific path-related operations for the Android
-- platform.
module FrontendPaths (getFilesDir, getCacheDir, runWithRef, viewFile) where

import qualified Data.Text as T
import qualified Data.Text.Encoding as T
import Control.Concurrent (threadDelay)
import Control.Monad (forever)
import Data.Default.Class (def)
import Data.IORef (IORef, newIORef, readIORef)
import Data.String (fromString)
import Language.Javascript.JSaddle (JSM)
import MainWidget (startMainWidget)
import System.IO (hSetBuffering, stdout, stderr, BufferMode(LineBuffering))
import qualified Android.HaskellActivity as Android
import PlatformEvents (PlatformEventCallbacks(..))

-- | Get the path to a directory that is private to the application where
-- internal application cache data may be written (and read back).
--
-- The application requires no special permissions to use this directory.  If
-- the app is uninstalled the directory and its children will be deleted.
--
-- https://developer.android.com/reference/android/content/Context#getCacheDir()
getCacheDir :: IO (Maybe FilePath)
getCacheDir = Android.getHaskellActivity >>= Android.getCacheDir

-- | Get the path to a directory that is private to the application where
-- internal application data may be written (and read back).
--
-- The application requires no special permissions to use this directory.  If
-- the app is uninstalled the directory and its children will be deleted.
--
-- https://developer.android.com/reference/android/content/Context#getFilesDir()
getFilesDir :: IO (Maybe FilePath)
getFilesDir = Android.getHaskellActivity >>= Android.getFilesDir

-- | Instantiate the frontend hooked up to Android-specific platform event
-- sources.  The frontend is responsible for populating an IORef with a
-- structure holding its handlers for those events.
runWithRef ::
  -- | Create the frontend.
  (Maybe (IORef PlatformEventCallbacks) -> JSM ()) ->
  -- | Run thhe frontend.
  IO ()
runWithRef makeJSM = do

  print "Setting up custom Android callbacks..."

  -- Create a place for a function that will get activity results delivered to
  -- it.  Start with a function that throws them away because we're not ready
  -- to deal with anything yet.  App setup will replace this when it is able.
  activityCallbackRef <- newIORef def

  hSetBuffering stdout LineBuffering
  hSetBuffering stderr LineBuffering
  Android.continueWithCallbacks $ def
    { Android._activityCallbacks_onCreate = \_ -> do
        a <- Android.getHaskellActivity
        let startPage = fromString "file:///android_asset/index.html"
        startMainWidget a startPage (makeJSM (Just activityCallbackRef))
    , Android._activityCallbacks_onBackPressed = do
          f <- readIORef activityCallbackRef
          onBackPressed f
    }
  forever $ threadDelay 1000000000


-- | Allow the operator of the device to view the contents of the file at the
-- given path.
--
-- TODO: Make it work.
viewFile :: FilePath -> IO ()
viewFile p = do
  print $ "Going to view " <> p
  act <- Android.getHaskellActivity
  Android.createViewIntent act (T.encodeUtf8 . T.pack $ p)
  print $  "I viewed " <> p
