The process of creating icons is documented at https://developer.android.com/studio/write/create-app-icons#access.

I started with the Tahoe-LAFS logo file (see `../../../static/`).

I used a scaling of 79 % (trimming `on`) to make sure our logo wouldn't be truncated by launchers (~= window managers on android) - i.e. respect the (66 dp)² "safe zone" in the center of the (108 dp)² icon.
