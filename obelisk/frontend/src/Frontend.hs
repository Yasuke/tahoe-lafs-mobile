{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE ScopedTypeVariables #-}

module Frontend where

import Data.IORef
import Control.Monad.IO.Class
import Obelisk.Frontend
import Obelisk.Route
import Obelisk.Route.Frontend
import Reflex.Dom.Core
import Common.Route
import Controller (pageFromRoute)
import Static
    ( mainCss
    , materialComponentsCss
    , materialComponentsJs
    , materialIconsCss
    , robotoCss
    )
import PlatformEvents (PlatformEventCallbacks(..), PlatformEvent(..))
import JSDOM.Types (MonadJSM, liftJSM)
import Navigation (platformEventCallback)
import Data.Default.Class (def)
import App (initialApp)

-- A frontend with no platform event integration.  This is necessary for
-- certain Obelisk integration (eg `ob run`) but is not used otherwise.
frontend :: Frontend (R FrontendRoute)
frontend = makeFrontend Nothing

-- Build the frontend.  If there is a place for it, supply handlers for the
-- supported platform events.
--
-- This runs in a monad that can be run on the client or the server.
-- To run code in a pure client or pure server context, use one of the
-- `prerender` functions.
makeFrontend :: Maybe (IORef PlatformEventCallbacks) -> Frontend (R FrontendRoute)
makeFrontend callbackRef = Frontend
  { _frontend_head = do
      el "title" $ text "Tahoe-LAFS Mobile"

      -- Required for proper resolution / font size
      elAttr "meta" ("name" =: "viewport" <> "content" =: "width=device-width, initial-scale=1.0") blank

      -- Include external Material 2 assets
      el "style" $ text $ "@import \"" <> materialIconsCss <> "\" layer;"
      el "style" $ text $ "@import \"" <> materialComponentsCss <> "\" layer;"
      el "style" $ text $ "@import \"" <> robotoCss <> "\" layer;"

      elAttr "script" (
        "src" =:
        -- When debugging MDC JavaScript issues, it is possibly nice to use the
        -- unminified source, for which a source map also exists.  However, note
        -- that there is no guarantee that this version will match the version
        -- we bundle!
        --
        -- "https://unpkg.com/material-components-web@latest/dist/material-components-web.js"
        --
        -- For real builds we want the local version:
        materialComponentsJs
        ) blank

      -- Our own resources follow to override earlier imports
      elAttr "link" ("href" =: mainCss <> "type" =: "text/css" <> "rel" =: "stylesheet") blank

  -- Obelisk.Route.Frontend.RoutedT t (R FrontendRoute) m ()
  , _frontend_body = do
      prerender_ blank (wirePlatformInputs callbackRef)

      startEv <- getPostBuild
      app <- initialApp startEv

      subRoute_ (pageFromRoute app)
  }

-- | Connect platform-specific inputs to the appropriate handlers.
wirePlatformInputs
    :: ( MonadIO m
       , TriggerEvent t m
       , PerformEvent t m
       , MonadJSM (Performable m)
       )
    => Maybe (IORef PlatformEventCallbacks)
    -> m ()
wirePlatformInputs callbackRef =
  case callbackRef of
    -- There is nowhere to put platform event integration, so do
    -- nothing.
    Nothing -> blank
    Just ref' -> do
      -- Create an event/function pair for integrating with a "back"
      -- navigation button.
      (backPressedEv, backPressedF) <- newTriggerEvent

      -- Put our event trigger function into the platform event
      -- integration structure.  This causes the Reflex event to trigger
      -- at the right time, then we can handle that in the usual way.
      liftIO $ writeIORef ref' (def { onBackPressed = backPressedF () })

      -- React to the "back" event by dispatching to some platform-specific
      -- handler.
      --
      -- Since a ... JSM ... exception coming out of the action might put the
      -- app in a broken state, it would be nice to handle such exceptions
      -- before they get to performEvent_ here.  However, I don't know how.
      -- platformEventCallback *should* be pretty safe since all it does is
      -- trigger a Reflex Event.
      performEvent_ $ ffor backPressedEv $ \() -> liftJSM $
          platformEventCallback BackPressed

-- mapRoutedT :: (m a -> n b) -> RoutedT t r m a -> RoutedT t r n b
-- subRoute_ ::
--    (MonadFix m, MonadHold t m, GEq r, Adjustable t m) =>
--    (forall a. r a -> RoutedT t a m ()) -> RoutedT t (R r) m ()
