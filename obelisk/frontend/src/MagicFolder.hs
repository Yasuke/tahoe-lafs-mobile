{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE NamedFieldPuns #-}
{-# LANGUAGE ScopedTypeVariables #-}

module MagicFolder where

import qualified Data.ByteString as B
import Data.Bifunctor (first)
import Text.Megaparsec (parse)
import qualified Data.Map.Strict as Map
import qualified Tahoe.Directory as Directory
import qualified Tahoe.SDMF as SDMF
import qualified Data.Text as T
import Data.Yaml (decodeEither')
import Tahoe.Announcement (Announcements(..))
import Control.Monad.IO.Class (MonadIO, liftIO)
import Control.Exception (SomeException, try)
import qualified Tahoe.MagicFoldr as MF
import FrontendPaths (getFilesDir)
import Obelisk.Configs (HasConfigs(getConfig))

data MagicFolder =
  MagicFolder
  { magicFolderName :: T.Text
  , magicFolderState :: MF.MagicFolder
  } deriving (Eq, Ord, Show)

-- | The name of some Magic-Folder entry (file, directory, etc) in the context
-- of its container (parent directory).
newtype EntryName = EntryName { unEntryName :: T.Text } deriving (Eq, Ord, Show)

-- | Load the announcements for the grid to use for downloads.
--
-- This reads the correct "servers.yaml" from the application-private files
-- directory.
loadGrid :: HasConfigs m => m (Either T.Text Announcements)
loadGrid = do
  announcementsBytes <- getConfig "common/production-servers.yaml"
  case announcementsBytes of
    Nothing -> pure . Left $ "Could not load grid configuration"
    Just bytes ->
      pure . first renderError $ decodeEither' bytes
      where
        renderError = T.concat . ("Could not parse grid configuration: ":) . (:[]) . T.pack . show

-- | Load the configured magic-folder collective capabilities.
--
loadFolderCollectives :: MonadIO m => m (Either T.Text (Map.Map T.Text (Directory.DirectoryCapability SDMF.Reader)))
loadFolderCollectives = do
  appdir <- liftIO getFilesDir
  case appdir of
    Just fd -> do
      let fname = fd <> "/magicfolders.yaml"
      collectivesBytes <- liftIO $ try $ B.readFile fname
      case collectivesBytes of
        -- todo: too broad
        Left (_ :: SomeException) -> pure $ Right mempty
        Right bytes ->
          case decodeEither' bytes of
            Left err -> pure $ Left $ T.concat ["Could not parse magic-folder collectives: ", T.pack $ show err]
            Right ann -> pure $ Right $ snd $ Map.mapEither (parse Directory.pReadSDMF "magic-folder collective capability") ann
    Nothing -> pure $ Left "Can't find app config dir"

-- | Load all of the locally configured/known Magic-Folders.  Use the
-- Magic-Foldr library representation of the folders.
loadMagicFolders' :: MonadIO m => m (Either T.Text (Map.Map T.Text MF.MagicFolder))
loadMagicFolders' = ((flip MF.MagicFolder Nothing <$>) <$>) <$> loadFolderCollectives


-- | Convert map of folders to a list of folders
convertMagicFolders :: Map.Map T.Text MF.MagicFolder -> [MagicFolder]
convertMagicFolders themap = go (Map.toList themap) where
  go [] = []
  go ((n, f):fs) = MagicFolder n f : go fs


-- | Load all of the locally configured/known Magic-Folders.  Use our
-- representation of the folders.
loadMagicFolders :: MonadIO m => m (Either T.Text [MagicFolder])
loadMagicFolders = do
  folders <- loadMagicFolders'
  pure $ case folders of
    Left e -> Left e
    Right f -> Right $ convertMagicFolders f

-- | Read the complete directory listing of one Magic-Folder.
readMagicFolder :: MonadIO m => Announcements -> MagicFolder -> m MF.FolderEntry
readMagicFolder (Announcements ann) MagicFolder{magicFolderState} = do
  root <- MF.getFolderFiles ann magicFolderState
  case root of
    (Right d@(MF.Directory _)) -> pure d
    _ -> undefined
