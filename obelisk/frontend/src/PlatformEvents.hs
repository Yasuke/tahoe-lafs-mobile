module PlatformEvents (
  PlatformEvent(..),
  PlatformEventCallbacks(..),
 ) where

import Data.Default.Class (Default(def))

-- | Represent some platform event that happened.
data PlatformEvent = BackPressed deriving (Eq, Ord, Show)

-- | Bundle up the different application-supplied handlers for different
-- platform events.
newtype PlatformEventCallbacks = PlatformEventCallbacks
    { onBackPressed :: IO ()
    }

-- | A default instance that does nothing for all platform events.
instance Default PlatformEventCallbacks where
  def = PlatformEventCallbacks
        { onBackPressed = pure ()
        }
