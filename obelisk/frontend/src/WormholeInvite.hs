{-# LANGUAGE OverloadedStrings #-}

module WormholeInvite (InviteMessage(..), acceptFolderInvite, receiveInvite) where

import qualified Data.ByteString as B
import qualified Data.ByteString.Lazy as LBS
import qualified Data.Text as T
import qualified Data.Text.Encoding as T

-- magic-wormhole invites related imports
import MagicWormhole
import MagicWormhole.Internal.Messages (Mood(..))
import Crypto.Spake2 (makePassword)
import Data.Text (breakOn, tail)
import GHC.Conc (atomically)
import Data.Aeson.Types (ToJSON(..), FromJSON(..), (.=), (.:), toJSON, parseJSON, object)
import qualified Data.Aeson.Types as AesonTypes
import qualified Data.Aeson as A
import Control.Exception (Exception, throwIO)
import Control.Monad.IO.Class (liftIO)

-- | Parsing the magic-folder invitation failed.
newtype InviteParseError = InviteParseError B.ByteString deriving (Show, Eq)
instance Exception InviteParseError

-- | Represent the version of a magic-folder invitation.
data InvitesVersion0 = InvitesVersion0 deriving (Eq, Show)

data InviteMessage = InviteMessage {
  -- This must always be "invite-v1".
  kind :: T.Text,
  folderName :: T.Text,
  collectiveReadCap :: T.Text
  } deriving (Eq, Show)


instance FromJSON InviteMessage where
  parseJSON (AesonTypes.Object v) =
    InviteMessage <$> v .: "kind" <*> v .: "folder-name" <*> v .: "collective"
  parseJSON _ = error "fixme"


-- | Complete a Magic-Folder invitation conversation as a receiver over the
-- given wormhole connection.
performInvite :: EncryptedConnection -> IO InviteMessage
performInvite conn = do
  MagicWormhole.PlainText invite_msg <- atomically $ receiveMessage conn
  -- todo: should check we got a "read-only" invite
  print invite_msg
  sendMessage conn (MagicWormhole.PlainText "{\"protocol\": \"invite-v1\", \"kind\": \"join-folder-accept\"}")
  MagicWormhole.PlainText ack_msg <- atomically $ receiveMessage conn
  print ack_msg
  -- pure $ fromMaybe (InviteMessage "invite-v1" "foo" "dummy") (A.decode (LBS.fromStrict invite_msg))
  -- not ^ because we want an error
  case A.decode (LBS.fromStrict invite_msg) of
    Nothing -> throwIO $ InviteParseError invite_msg
    Just x -> pure x


instance ToJSON InvitesVersion0 where
  toJSON InvitesVersion0 =
    object ["magic-folder" .= object ["supported-messages" .= ["invite-v1" :: String]]]

instance FromJSON InvitesVersion0 where
  parseJSON (AesonTypes.Object v) = do
    (AesonTypes.Object _mf) <- v .: "magic-folder"
--    supported <- mf .: "supported-messages"
    return InvitesVersion0
  parseJSON _ = error "fixme"


-- prototyping higher-level API for haskell-magic-wormhole
-- "a" = InviteMessage for us
-- "j" is InvitesVersion0, or app-versions type
-- doWormholeInteraction :: j -> (EncryptedConnection -> IO a) -> T.Text -> IO a
-- doWormholeInteraction _appversion _interact _code = undefined

-- "even higher", we can do a magic-folder invite
-- inviteToMagicFolder :: T.Text -> IO InviteMessage
-- inviteToMagicFolder = doWormholeInteraction InvitesVersion0 performInvite
-- equiv: inviteToMagicFolder code = doWormholeInteraction InvitesVersion0 performInvite code

receiveInvite :: T.Text -> T.Text -> MagicWormhole.Session -> IO InviteMessage
receiveInvite nameplate code session = do
  mailbox <- liftIO $ MagicWormhole.claim session (MagicWormhole.Nameplate nameplate)
  peer <- liftIO $ MagicWormhole.open session mailbox
  let passcode = T.encodeUtf8 . T.append (T.append nameplate "-") $ code
  result <- liftIO $ MagicWormhole.withEncryptedConnection peer (Crypto.Spake2.makePassword passcode) InvitesVersion0 performInvite
  MagicWormhole.release session (Just (MagicWormhole.Nameplate nameplate))
  _x <- MagicWormhole.close session (Just mailbox) (Just Happy)
  pure result

-- | Take a code like "1-foo-bar" and split off the mailbox, returning a tuple
-- like ("1", "foo-bar")
splitWormholeCode :: T.Text -> (T.Text, T.Text)
splitWormholeCode entireCode = do
  let (n, c) = breakOn (T.pack "-") entireCode
  let code = Data.Text.tail c
  (n, code)

-- | public API
acceptFolderInvite :: T.Text -> IO InviteMessage
acceptFolderInvite code = do
  side <- MagicWormhole.generateSide
  MagicWormhole.runClient endpoint appID side Nothing (receiveInvite nameplate onlycode)
  where
    -- codes come in like: "1-foo-bar"
    (nameplate, onlycode) = splitWormholeCode code

    -- "ws://relay.magic-wormhole.io:4000/v1"
    endpoint = MagicWormhole.WebSocketEndpoint "relay.magic-wormhole.io" 4000 "/v1"

    appID = MagicWormhole.AppID "private.storage/magic-folder/invites"
