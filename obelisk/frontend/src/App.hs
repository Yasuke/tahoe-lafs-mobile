{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE TupleSections #-}
{-# LANGUAGE RecursiveDo #-}
{-# LANGUAGE ScopedTypeVariables #-}

-- | Definitions related to the application-wide model.

module App where

import Tahoe.MagicFoldr (FolderEntry(..))
import qualified Data.Map.Strict as Map
import qualified Data.Text as T
import Control.Exception (SomeException)
import Control.Monad (join)
import Control.Monad.Fix (MonadFix)
import Control.Monad.IO.Class (MonadIO)
import Reflex.Class (MonadHold(holdDyn), Dynamic)
import Reflex.TriggerEvent.Class (TriggerEvent)
import Reflex
    ( PerformEvent(performEvent)
    , Performable
    , Event
    , ffor
    , newTriggerEvent
    , attachWith
    , current
    , foldDyn
    , leftmost
    , fanEither
    , fforMaybe
    )
import Obelisk.Configs (HasConfigs)
import Tahoe.Announcement (Announcements)
import MagicFolder (MagicFolder, loadGrid, loadMagicFolders, readMagicFolder)
import PerformEvent (performEventInThread)

-- | The combined Tahoe-LAFS Mobile changeable application state.
data App t = App
    { -- | The Tahoe-LAFS storage servers the application will use.
      appGrid :: Dynamic t (Either T.Text Announcements)
    , -- | The Magic-Folders the application is aware of.
      appFolders :: Dynamic t (Either T.Text [MagicFolder])
    , -- | The contents of each Magic-Folder if we have recently loaded them
      appFolderContents :: Dynamic t (Map.Map MagicFolder FolderEntry)
    , -- | When True, we've loaded our magic-folders (possibly none of them)
      appInitialized :: Dynamic t (Bool, Either T.Text [MagicFolder])
    , -- | A function which will trigger an update of the contents of one Magic-Folder.
      appUpdateFolder :: MagicFolder -> IO ()
    , -- | A function which triggers a reload of the list of all folders
      appUpdateFoldersList :: Either T.Text [MagicFolder] -> IO ()
    }

-- | Load the initial application state from local configuration.
initialApp
    :: forall t m.
       ( MonadHold t m
       , MonadFix m
       , PerformEvent t m
       , TriggerEvent t m
       , MonadIO (Performable m)
       , HasConfigs (Performable m)
       )
    => Event t ()     -- ^ An event to trigger loading the state.
    -> m (App t)      -- ^ The loaded application state.
initialApp startEv = do
      -- There is no IO involved in loadGrid currently so we don't use the
      -- threaded/safe performEventInThread.
      gridEv <- performEvent $ ffor startEv $ const loadGrid
      gridDyn <- holdDyn (Left "Loading ...") gridEv

      (updateFoldersListEv, updateFoldersList) <- newTriggerEvent
      magicFoldersDyn <- holdDyn (Left "loading ..") updateFoldersListEv

      -- update the folder list immediately at startup
      loadCompleteEv <- performEventInThread $ ffor startEv $ \() -> do
        f <- loadMagicFolders
        updateFoldersList f
        pure f

      let (firstLoadFailedEv :: Event t SomeException, firstLoadEvent) = fanEither loadCompleteEv

      -- in this case, we can only fire once but if this could fire
      -- multiple times we should always remain True once we get to
      -- True
      initializedDyn <- holdDyn (False, Right []) $
          leftmost
          [
            -- no meaningful state available because of some error
            (False, Right []) <$ firstLoadFailedEv
            -- state load attempt did complete, but could be with success or
            -- failure
          , (True,) . join . Right <$> firstLoadEvent
          ]

      -- Create a paired event/function for performing a content refresh on
      -- a specific magic-folder.
      (updateFolderEv, updateFolder) <- newTriggerEvent

      -- Updates to folderContentsDyn depend on the current value of
      -- folderContentsDyn (don't download if we already have the state) so we
      -- start a recursive block for its definition.
      rec
        -- Bundle up all of the pieces we need to perform a content refresh.
        -- We need to know the grid configuration so we can talk to some
        -- servers.  We want to know the current folder state so we can skip
        -- spurious refreshes.  And we need to know which folder to update.
        let
          -- Merge the two Dynamics and get the resulting Behavior - a
          -- function from one more value to a three-tuple.
          gridAndContents = current $ (,,) <$> gridDyn <*> folderContentsDyn
          -- In a new Event, finish building the tuple, taking the final value
          -- (the magic-folder to update) from the update folder event.
          gridContentsTarget = attachWith ($) gridAndContents updateFolderEv

        -- Create an event that triggers whenever the folder-update event we
        -- created above triggers, only if we need to perform a download for
        -- the folder in the event.
        let reloadNeededEv = fforMaybe gridContentsTarget $
                \case
                  (Left _, _, _) -> Nothing
                  (Right grid, contents, folder) ->
                    if folder `Map.member` contents
                    -- We already have its state, skip the operation.
                    then Nothing
                    -- It's missing, download its info.
                    else Just $ (folder,) <$> readMagicFolder grid folder

        -- Download the contents of any folder that meets the criteria.
        folderUpdateComplete <- performEventInThread reloadNeededEv
        -- TODO: If the load fails it would be nice to indicate this to the
        -- user somewhere.
        let (_folderUpdateErrorEv :: Event t SomeException, folderContentsChanged) = fanEither folderUpdateComplete

        -- Fold the folder state up over all of the content changes.
        folderContentsDyn <- foldDyn updateFolderContents mempty folderContentsChanged

      pure $ App gridDyn magicFoldersDyn folderContentsDyn initializedDyn updateFolder updateFoldersList

-- | Update the folder state associated with the given Magic-Folder.
updateFolderContents :: (MagicFolder, FolderEntry) -> Map.Map MagicFolder FolderEntry -> Map.Map MagicFolder FolderEntry
updateFolderContents = uncurry Map.insert
