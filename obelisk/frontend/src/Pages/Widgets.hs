{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RecursiveDo #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE LambdaCase #-}

module Pages.Widgets (errorBox, mdcButton, mdcDialog, mdcText) where

import Data.Maybe (isJust)
import qualified Data.Map.Strict as Map
import qualified Data.Text as T
import Reflex.Dom.Core


-- | Show Just an error message on the page or nothing when the error message
-- is Nothing.  Also return an Event that triggers when the error should be
-- dismissed.
errorBox
    :: ( PostBuild t m
       , DomBuilder t m
       )
    => Dynamic t (Maybe T.Text)
    -> m (Event t ())
errorBox msg = do
  (_, _, closeButton) <- mdcDialog visible (text "Error") (dynText msg') $ do
    (closeButton, _) <- mdcButton "close" $ do
      divClass "mdc-button__ripple" blank
      divClass "mdc-button__label" $ text "Dismiss"
    pure closeButton
  pure $ domEvent Click closeButton
  where
    visible = isJust <$> msg
    msg' = ffor msg $ \case
      Nothing -> ""
      Just txt -> txt


-- | An MDC-themed dialog widget.
mdcDialog
    :: (DomBuilder t m, PostBuild t m)
    => Dynamic t Bool
    -- ^ A dynamic of whether the dialog is visible or not.
    -> m title
    -- ^ A widget for the dialog title.
    -> m content
    -- ^ A widget for the dialog content.
    -> m actions
    -- ^ A widget for any actions (eg, "ok" / "cancel" buttons) the dialog
    -- has.
    -> m (title, content, actions)
    -- ^ A widget with the results of the components.
mdcDialog visible title content actions =
    elDynAttr "div" topLevelDynAttrs $ do
      divClass "mdc-dialog__container" $ do
        divClass "mdc-dialog__surface" $ do
          titleValue <- elClass "h2" "mdc-dialog__title" title
          (contentValue, actionsValue) <- divClass "mdc-dialog__content" $ do
            contentValue <- content
            actionsValue <- divClass "mdc-dialog__actions" actions
            pure (contentValue, actionsValue)
          pure (titleValue, contentValue, actionsValue)
      <* divClass "mdc-dialog__scrim" blank

    where
      topLevelDynAttrs = attributes' <$> visible

      attributes' :: Bool -> Map.Map T.Text T.Text
      -- Auto-initialize in "closed" state, then change class to
      -- "mdc-dialog--open" to show the dialog in a modal fashion (with an
      -- overlay temporarily disabling all other elements).
      attributes' True = commonAttrs <> "class" =: "mdc-dialog mdc-dialog--open"
      attributes' False = commonAttrs <> "class" =: "mdc-dialog mdc-dialog--closed"

      commonAttrs = "data-mdc-auto-init" =: "MDCDialog"


-- | An MDC-themed text field widget.
mdcText
    :: DomBuilder t m
    => m a
    -- ^ A widget of the unstyled text.
    -> m a
    -- ^ The text field widget with the result of the wrapped widget.
mdcText = elAttr "label" attrs
  where
    attrs
        = "class" =: "mdc-text-field mdc-text-field--filled"
        <> "data-mdc-auto-init" =: "MDCTextField"


-- | An MDC-themed button.
mdcButton
    :: DomBuilder t m
    => T.Text
    -- ^ The text on the button.
    -> m a
    -- ^ A widget that will be made a child of the button.
    -> m (Element EventResult (DomBuilderSpace m) t, a)
    -- ^ The button element and the result of the child widget.
mdcButton label =
  elAttr' "button" ("type" =: "button" <> "class" =: "mdc-button mdc-dialog__button" <> "data-mdc-dialog-action" =: label)
