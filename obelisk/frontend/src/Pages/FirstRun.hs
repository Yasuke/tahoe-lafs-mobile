{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE PatternSynonyms #-}

module Pages.FirstRun (firstRunPage) where

import Reflex.Dom.Core

import Static (logo)

import Obelisk.Route.Frontend (setRoute, SetRoute(..), R)
import Obelisk.Route (pattern (:/))
import Common.Route


-- Define the entire view for looking at our beautiful firstRun screen.
firstRunPage :: (SetRoute t (R FrontendRoute) m, DomBuilder t m) => m ()
firstRunPage = do
  elClass "main" "firstRunPage" $ do

    elClass "div" "logo" $
      elAttr "img" ("src" =: logo) blank
    elClass "div" "subtext mdc-typography" $ do
      elClass "h1" "mdc-typography--headline3" $ text "Tahoe-LAFS"
      elClass "p" "mdc-typography--subtitle1" $ do
        text "A free and open decentralized"
        el "br" blank
        text "cloud storage system."

    (readyButton, _) <- elAttr' "button" ("class" =: "mdc-button mdc-button--raised" <> "data-mdc-auto-init" =: "MDCRipple") $ do
      elClass "span" "mdc-button__ripple" blank
      elClass "span" "mdc-button__label" $
        text "GET STARTED"

    setRoute $ (FrontendRoute_MagicFolders :/ ()) <$ domEvent Click readyButton

    el "script" $
      text "mdc.autoInit();"
