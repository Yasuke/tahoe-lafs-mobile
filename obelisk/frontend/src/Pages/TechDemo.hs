{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE ConstraintKinds #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE NamedFieldPuns #-}
{-# LANGUAGE TypeFamilies #-}

module Pages.TechDemo where

import Data.Either (fromRight, fromLeft)
import Tahoe.Capability (confidentiallyShow)
import Data.Void (Void)
import qualified Data.Map.Strict as Map
import Data.Bifunctor (Bifunctor(first))
import qualified Data.ByteString.Base64 as Base64
import qualified Tahoe.Directory as Directory
import Tahoe.Download (
  DownloadError,
  DirectoryDownloadError,
  download,
  downloadDirectory,
  announcementToImmutableStorageServer,
  announcementToMutableStorageServer
  )
import JSDOM (currentWindow)
import JSDOM.History (back)
import JSDOM.Window (getHistory)
import qualified Data.ByteString.Lazy as LBS
import qualified Data.ByteString as B
import qualified Data.Text as T
import qualified Data.Text.IO as T
import qualified Data.Text.Encoding as T
import qualified Data.Text.Encoding.Error as T
import qualified Data.Yaml as Yaml
import Data.ByteString.Base32 (encodeBase32Unpadded)
import Control.Monad.IO.Class (MonadIO, liftIO)
import Data.List.NonEmpty (NonEmpty(..))
import Network.HTTP.Req (HttpException, GET(GET), NoReqBody(NoReqBody), https, req, ignoreResponse, responseHeader, runReq, defaultHttpConfig)
import Data.Maybe (fromMaybe)
import Control.Monad (void)
import Control.Exception (try)
import Data.Serialize (encode)
import Obelisk.Route.Frontend hiding (encode)

import WormholeInvite (acceptFolderInvite, InviteMessage(..))
import qualified Tahoe.CHK.Capability as CHK
import qualified Tahoe.CHK.Upload as CHK
import qualified Tahoe.CHK.Types as CHK
import Tahoe.CHK.Server (StorageServer)
import Tahoe.Announcement (Announcements(..))
import Tahoe.Server (nullStorageServer)
import qualified Tahoe.SDMF as SDMF
import Reflex.Dom.Core
import Language.Javascript.JSaddle (eval, liftJSM)
import Text.Megaparsec (ParseErrorBundle, Parsec, parse, (<|>))
import FrontendPaths (getFilesDir)
import Common.Route
import MagicFolder (MagicFolder(..), loadMagicFolders)

techDemoPage
    :: ( PerformEvent t m
       , Prerender t m
       , SetRoute t (R FrontendRoute) (Client m)
       )
    => Dynamic t (Either T.Text Announcements)
    -> Dynamic t (Either T.Text [MagicFolder])
    -> (Either T.Text [MagicFolder] -> IO ())
    -> Dynamic t [T.Text]
    -> m ()
techDemoPage gridDyn foldersDyn updateFolders locationDyn = prerender_ (pure ()) $ do
    el "h1" $ text "Tech Demo"

    -- The API for encoding a CHK requires some servers to upload to.  We
    -- don't have any real servers to upload to yet but we can make up this
    -- no-op server that's good enough.  This "behavior" value always has a
    -- NonEmpty of StorageServers.  We can pass it around to any API that
    -- needs servers and the value can be sampled whenever it is needed.
    -- This centralizes responsibility for knowing which servers to use
    -- here.
    --
    -- we represent this as a NonEmpty because the current implementation
    -- errors on an empty server list.  Also it is not clear what it really
    -- means to upload to no servers.
    let serversBehavior = constant (nullStorageServer :| [])
        -- It also requires some encoding parameters.  Handle those
        -- likewise.
        paramsBehavior = constant CHK.Parameters
                         -- Note this is an unreasonably small segment size.
                         { CHK.paramSegmentSize = 128
                         , CHK.paramRequiredShares = 7
                         , CHK.paramHappyShares = 1
                         , CHK.paramTotalShares = 11
                         }

    -- Place an input field into the page and retain a reference to it.
    el "span" $ text "Encode some plaintext: "
    (_, inputWidget) <- el' "span" $ inputElement def

    -- Get an event that triggers any time the contents of the input field
    -- change (a lot of changes!).  It triggers with the utf-8 encoded
    -- representation of the contents.
    let plaintextEv = LBS.fromStrict . T.encodeUtf8 <$> updated (_inputElement_value inputWidget)

    -- Use the behaviors and the plaintext input event to encode the data
    -- and make an event that triggers whenever this is done, with the
    -- "upload result" (quirk of the current tahoe-chk API) as its value.
    uploadedEv <- uploadEv serversBehavior paramsBehavior plaintextEv

    -- Get an event that triggers with the full string representation of the
    -- capability resulting from encoding the plaintext.
    let theCapTextEv = CHK.dangerRealShow . CHK.CHKReader . CHK.uploadResultReadCap <$> uploadedEv

    -- Put the capability string into the page.
    theCapTextDyn <- holdDyn "" theCapTextEv
    el "p" $ dynText theCapTextDyn

    -- Show that we can parse capabilities as well as generate them.
    el "p" parseCapDemo

    -- Do some file I/O and put the results on the page.
    el "p" filesDemoWidget

    -- Some network I/O
    el "p" networkWidget

    -- Demonstrate Obelisk "pre-rendering".
    el "p" prerenderWidget

    -- Demonstrate a rangeInput which, for some reason, can only be rendered
    -- in a client context.
    el "p" $ do
      el "div" $ text "Here is a slider for you."
      el "div" $ do
          rg <- rangeInput def
          text "⇉"
          el "span" $ dynText $ T.pack . show <$> _rangeInput_value rg

    -- Try to do some wormhole stuff
    el "div" $ wormholeInviteWidget updateFolders

    -- Add some buttons to get out of here.
    jump "main" (FrontendRoute_Main :/ ())
    jump "magic-folder list" (FrontendRoute_MagicFolders :/ ())
    jump "file browser" (FrontendRoute_FileBrowser :/ ("foo", ["bar"]))

    -- Demo jsaddle-dom-based back / forward navigation
    el "div" navigationDemo

    el "div" $ dyn_ $ downloadWidget <$> gridDyn
    el "div" $ dyn_ $ directoryWidget <$> gridDyn <*> locationDyn

    el "div" $ do
      base64InputWidget <- inputElement def
      dynText $ T.pack . show . Base64.encode . T.encodeUtf8 <$> _inputElement_value base64InputWidget

    el "div" $ do
      el "span" $ text "Your magic-folders: "
      dynText $ T.intercalate "," . fmap magicFolderName . fromRight mempty <$> foldersDyn
    el "div" $ do
      el "span" $ text "Your magic-folder errors: "
      dynText $ fromLeft "<none>" <$> foldersDyn

fromEither :: Either a a -> a
fromEither (Left a) = a
fromEither (Right a) = a

-- | Render a "Back" button hooked up to the browser history navigation action "back".
navigationDemo :: (Prerender t m, DomBuilder t m) => m ()
navigationDemo = do
    -- This renders a button into the page so there is something to see and
    -- click on.  It returns an `Event t ()` that triggers when the button is
    -- clicked.
    backEv <- button "Back"

    -- Only do the rest of this on the client.  There's no way to look up the
    -- browser Window or History when doing server-side rendering.
    prerender_ blank $

      -- Use performEvent_ to take an action on the Window whenever the event
      -- triggers.
      performEvent_ . ffor backEv $ \() -> do
        windowM <- currentWindow
        case windowM of
          -- No Window ... no navigation.
          Nothing -> blank
          Just window -> do
            history <- getHistory window
            back history

jump :: (DomBuilder t m, SetRoute t r m) => T.Text -> r -> m ()
jump label route = do
  b <- button $ "Jump to " <> label
  setRoute $ route <$ b
  pure ()


-- | Accept a capability string as input, parse it, and render something about
-- it.
parseCapDemo :: (DomBuilder t m, PostBuild t m) => m ()
parseCapDemo =
    el "div" $ do
      el "span" $ text "Parse a cap: "
      -- Place an input field into the page and retain a reference to it.
      capInputWidget <- inputElement def

      -- `Dynamic t T.Text` holding the value of the text input.
      let capTextDyn = _inputElement_value capInputWidget

      -- `Dynamic t (Either ParseErrorBundle s e) CHK` holding a parse error
      -- or a parsed capability.
      let parsedCap = parseCap <$> capTextDyn

      dyn_ (renderCap <$> parsedCap)

        where
          parseCap = parse CHK.pCapability ""


-- | The rendering part of parseCapDemo.
renderCap :: DomBuilder t m => Either (ParseErrorBundle s e) CHK.CHK -> m ()
renderCap (Left _) = text "(unparseable)"
renderCap (Right (CHK.CHKVerifier CHK.Verifier { CHK.storageIndex, CHK.fingerprint, CHK.required, CHK.total, CHK.size })) = do
  el "div" $ text ("Storage index: " <> T.toLower (encodeBase32Unpadded storageIndex))
  el "div" $ text ("Fingerprint: " <> T.toLower (encodeBase32Unpadded fingerprint))
  el "div" $ text ("Required shares: " <> T.pack (show required))
  el "div" $ text ("Total shares: " <> T.pack (show total))
  el "div" $ text ("Plaintext size: " <> T.pack (show size))

renderCap (Right (CHK.CHKReader r)) = do
  el "div" $ text ("Read key: " <> (T.toLower . encodeBase32Unpadded . encode . CHK.readKey) r)
  renderCap (Right . CHK.CHKVerifier . CHK.verifier $ r)

-- | Given an event of a plaintext create an event of the CHK capability
-- object representing that plaintext.  Note that since the data is not
-- uploaded anywhere the capability cannot actually be used to recover the
-- plaintext.
uploadEv ::
    (PerformEvent t m, MonadIO (Performable m)) =>
    -- | A behavior of the servers we could upload to.
    Behavior t (NonEmpty StorageServer) ->
    -- | A behavior of the encoding parameters we should use.
    Behavior t CHK.Parameters ->
    -- | An event that triggers with plaintext that we should encrypt and
    -- upload.
    Event t LBS.ByteString ->
    -- | An event that triggers with an upload result after an upload
    -- succeeds.
    m (Event t CHK.UploadResult)
uploadEv servers parameters plaintextEv = do

  -- Ignore empty strings, the encoder does not support them (XXX
  -- unfortunately it fails by going into an infinite loop).
  let nonEmptyPlaintext = (not . LBS.null) `ffilter` plaintextEv

  -- Attach upload parameters to the plaintext
  let paramsAndPlaintext = attach parameters nonEmptyPlaintext

  -- Whenever the plaintext changes, construct an "uploadable" value from it
  -- that can be used below.
  uploadableEv <-
      performEvent $ ffor paramsAndPlaintext $ \(params, plaintext) ->
          liftIO $ CHK.memoryUploadableWithConvergence "secret" (fromIntegral $ LBS.length plaintext) plaintext params


  -- Attach the server list to the uploadable value
  let inputsReady = attach servers uploadableEv

  -- Run the actual upload of the uploadable to the current servers and return
  -- the result.
  performEvent $ ffor inputsReady $ \(aServer :| moreServers, uploadable) ->
      liftIO $ CHK.store (aServer:moreServers) uploadable


filesDemoWidget ::
        (Monad m, Reflex t, Prerender t m, PerformEvent t m)
        => m ()
filesDemoWidget =
  -- Rendering the main body of the files demo widget on the server and client
  -- (both part of the frontend) produces different results.  This causes a
  -- warning about "hydration failure" indicating reflex-dom has gotten
  -- confused by this difference.  There's no particular reason to render the
  -- files demo widget on the server.  We can use `prerender_ blank` to cause
  -- the server to render `blank` and let the client be the only renderer of
  -- the real content.
  prerender_ blank $ do
    (runButton, _) <- el' "button" $ text "Run File I/O"
    let runEv = domEvent Click runButton

    -- Demonstrate writing and reading data in an execution-context-defined
    -- application-private location.
    ev <- performEvent . ffor runEv . const . liftIO $
          do
            p <- getFilesDir
            case p of
              Nothing -> pure Nothing
              Just p' ->
                  do
                    -- Be sure to do strict IO here, which Data.Text.IO does,
                    -- otherwise the lazy write and the lazy read conflict
                    -- with each other in surprising ways (eg, resulting in
                    -- in "resource is busy" error from the Haskell runtime).
                    -- Another solution would be to use getContents or
                    -- withFile or some such.
                    T.writeFile path "Hello, world!"
                    content <- T.readFile path
                    pure (Just (p', content))
                  where
                    path = p' <> "/" <> "somedata.txt"

    filesDyn <- holdDyn Nothing ev
    el "p" $ dynText (T.pack . show <$> filesDyn)


-- | Demonstrate doing some network I/O in a way that's integrated with
-- Reflex-DOM.
networkWidget :: (DomBuilder t m, Prerender t m) => m ()
networkWidget =
  -- See filesDemoWidget for an explanation of the prerender_ call.
  prerender_ blank $ do
    (runButton, _) <- el' "button" $ text "Run Request"
    let runEv = domEvent Click runButton
    resultEv <- performEvent $ ffor runEv $ \_ -> liftIO getResult
    resultDyn <- holdDyn Nothing resultEv
    el "div" $ do
      el "span" $ text "Resource age:"
      el "span" $ dynText (fromMaybe "<unknown>" <$> resultDyn)
    where
      request = req GET (https "private.storage") NoReqBody ignoreResponse mempty
      getResult :: IO (Maybe T.Text)
      getResult = do
        respE <- try $ runReq defaultHttpConfig request
        case respE of
          Left err -> pure . Just . T.pack . ("Error: " <>) . show $ (err :: HttpException)
          Right resp ->
              case T.decodeUtf8' <$> responseHeader resp "age" of
                Nothing -> pure Nothing
                Just (Left err) ->
                  pure . Just . T.pack . ("Error: " <>) . show $ err
                Just (Right txt) ->
                  pure $ Just txt


-- | Accept an invitation to a magic-folder.
doWormholeInvite
    :: T.Text
    -- ^ The wormhole-code for the invitation.
    -> IO (Maybe T.Text)
    -- ^ If the invite is is successful, Just the read cap of the collective
    -- of the magic-folder.  Otherwise, Nothing.
doWormholeInvite citd = do
  inv <- acceptFolderInvite citd
  Just fd <- getFilesDir
  let fname = fd <> "/magicfolders.yaml"
  raw_existing <- try $ B.readFile fname
  case raw_existing of
    Right raw_data ->
      case Yaml.decodeEither' raw_data of
        Right existing -> do
          -- todo: handle conflicts
          let new_folders = Map.insert (folderName inv) (collectiveReadCap inv) existing
          Yaml.encodeFile fname new_folders

        Left bad -> error $ show bad

    -- duplicate-looking, but refactor this out later
    Left (_ :: IOError) -> do
      let new_folders = Map.insert (folderName inv) (collectiveReadCap inv) mempty
      Yaml.encodeFile fname new_folders

  pure $ Just (collectiveReadCap inv)


-- | magic-wormhole based inviters
wormholeInviteWidget :: (DomBuilder t m, Prerender t m) => (Either T.Text [MagicFolder] -> IO ()) -> m ()
wormholeInviteWidget updateFolders =
  -- See filesDemoWidget for an explanation of the prerender_ call.
  prerender_ blank $ do
    (inviteButton, codeInputTextDyn) <- el "div" $ do
      ci <- inputElement def
      let citd = _inputElement_value ci
      (ib, _) <- el' "button" $ text "Receive Invite"
      pure (ib, citd)
    let runEv = domEvent Click inviteButton
    let runEvWithText = tag (current codeInputTextDyn) runEv
    resultEv <- performEvent $ ffor runEvWithText $ \t -> liftIO $ do
      cap <- doWormholeInvite t
      mf <- loadMagicFolders
      updateFolders mf
      pure cap

    resultDyn <- holdDyn Nothing resultEv
    el "div" $ do
      el "span" $ text "Collective: "
      el "span" $ dynText (fromMaybe "<unknown>" <$> resultDyn)




prerenderWidget :: (Monad m, Prerender t m) => m ()
prerenderWidget =
    -- `prerender` and `prerender_` let you choose a widget to run on the server
    -- during prerendering and a different widget to run on the client with
    -- JavaScript. The following will generate a `blank` widget on the server and
    -- print "Hello, World!" on the client.
    prerender_ blank $ liftJSM $ void $ eval ("console.log('Hello, World!')" :: T.Text)

data WhichCap = CHKCap CHK.Reader | SDMFCap SDMF.Reader

pCap :: Parsec Void T.Text WhichCap
pCap = (CHKCap <$> CHK.pReader) <|> (SDMFCap <$> SDMF.pReader)

downloadWidget :: (MonadIO (Performable m), PerformEvent t m, DomBuilder t m, MonadHold t m, PostBuild t m) => Either T.Text Announcements -> m ()
downloadWidget anns = do
  -- A place to type the cap
  (_, inputWidget) <- el' "span" $ inputElement def
  goEv <- button "Download"

  -- Attach the value of the input to the download button event
  let capTextEv = tag (current $ _inputElement_value inputWidget) goEv

  -- Get separate events for failures and successes so we can render them separately.
  let (parseErrorEv, capEv) = fanEither (parse pCap "CHK/SDMF read cap" <$> capTextEv)

  -- Track parse errors
  parseErrorDyn <- holdDyn "" (T.pack . show <$> parseErrorEv)

  -- Run downloads
  plaintextEv <- performEvent $ ffor capEv $ \cap -> liftIO $ do
    case anns of
      Right (Announcements grid) -> do
        r <- case cap of
          CHKCap r -> download grid r announcementToImmutableStorageServer
          SDMFCap r -> download grid r announcementToMutableStorageServer
        -- Process it for display
        case r of
          Left err -> pure . Left . DownloaderError $ (err :: DownloadError)
          Right bs -> pure . first DecodingError . T.decodeUtf8' . LBS.toStrict $ bs
      Left e -> pure $ Left (ConfigurationError e)

  -- Separate success from error
  let (downloadErrEv, downloadSuccessEv) = fanEither plaintextEv

  -- Track download errors
  downloadErrorDyn <- holdDyn "" (T.pack . show <$> downloadErrEv)

  -- Show the latest errors
  el "div" $ do
    dynText parseErrorDyn
    dynText downloadErrorDyn

  -- Render plaintext on success
  plaintextDyn <- holdDyn "" downloadSuccessEv
  el "div" $
      el "pre" (dynText plaintextDyn)

-- | The download widget encountered an error.
data DownloadWidgetError
    = DownloaderError DownloadError
    -- ^ There was a problem downloading the data associated with the capability.
    | DirectoryDownloaderError DirectoryDownloadError
    -- ^ As above but for a directory.
    | DecodingError T.UnicodeException
    -- ^ There was a problem decoding the downloaded data for display on the page.
    | ConfigurationError T.Text
    -- ^ There was a problem with configuration required by the downloader.
    deriving (Eq, Show)


-- | Constraints required for doing all of the directory demo stuff.
type DirectoryContext t m
    = ( MonadIO (Performable m)
      , SetRoute t (R FrontendRoute) m
      , PerformEvent t m
      , DomBuilder t m
      , MonadHold t m
      , PostBuild t m
      )

-- | Render a text input for a directory cap and then render the contents of
-- the entered directory.  Navigation here uses routes so back/forward buttons
-- work to return to a previous directory or go forward again.
directoryWidget :: DirectoryContext t m => Either T.Text Announcements -> [T.Text] -> m ()
directoryWidget _ [] = textEntryDirectoryWidget
directoryWidget grid (dirCap:_) = subDirectoryWidget grid dirCap

-- | Render the initial text input for a directory cap and start the
-- navigation process.
textEntryDirectoryWidget :: DirectoryContext t m => m ()
textEntryDirectoryWidget = do
  -- A place to type the cap
  (_, inputWidget) <- el' "span" $ inputElement def
  goEv <- button "Browse SDMF Directory"

  -- Attach the value of the input to the download button event
  let capTextEv = tag (current $ _inputElement_value inputWidget) goEv

  -- Get separate events for failures and successes so we can render them separately.
  let (parseErrorEv, capEv) = fanEither (parse Directory.pReadSDMF "DIR2 read cap" <$> capTextEv)

  -- Track parse errors
  parseErrorDyn <- holdDyn "" (T.pack . show <$> parseErrorEv)

  -- Show the latest errors
  el "div" $ dynText parseErrorDyn

  -- On success, update the route.
  setRoute $ (\cap -> FrontendRoute_TechDemo :/ [confidentiallyShow cap]) <$> capEv

-- | Render the children of some directory.  Directory children are clickable
-- for further navigation.
subDirectoryWidget :: forall t m. DirectoryContext t m => Either T.Text Announcements -> T.Text -> m ()
subDirectoryWidget grid dirCapText = do
  -- Run the download
  ev <- getPostBuild
  downloadedEv <- performEvent $ ffor (grid <$ ev) $ \anns -> liftIO $ do
    case anns of
      Right (Announcements grid') ->
        case parse Directory.pReadSDMF "DIR2 read cap" dirCapText of
          Right cap -> first DirectoryDownloaderError <$> downloadDirectory grid' cap announcementToMutableStorageServer
          Left e -> pure $ Left (ConfigurationError $ T.pack $ show e)
      Left e -> pure $ Left (ConfigurationError e)

  -- Separate success from error
  let (downloadErrEv, downloadSuccessEv) = fanEither downloadedEv

  -- Process success for display
      entriesEv :: Event t [m ()] = ffor downloadSuccessEv $ \(Directory.Directory children) -> renderEntry <$> children

  -- Track download errors
  downloadErrorDyn <- holdDyn "" (T.pack . show <$> downloadErrEv)

  -- Show the latest errors
  el "div" $ dynText downloadErrorDyn

  -- Render plaintext on success
  let combinedEntriesEv :: Event t (m ()) = sequence_ <$> entriesEv

  el "div" $ do
    el "div" $ text $ "Directory: " <> dirCapText
    el "div" $ text "Children: "
    el "div" $ widgetHold_ (text "Loading ...") combinedEntriesEv

-- | Render a single directory entry.  All files are rendered as just their
-- name but directories are also clickable for navigation.
renderEntry :: (DomBuilder t m, SetRoute t (R FrontendRoute) m) => Directory.Entry -> m ()
renderEntry Directory.Entry{Directory.entryName,Directory.entryReader} =
  case parse Directory.pReadSDMF ("entry " <> T.unpack entryName) readerText of
    Right _cap -> do
      -- It was an SDMF directory readcap.  Let the user navigate into it.
      (dirEl, _) <- el' "div" $ elAttr "span" ("style" =: "color: blue;") $ text entryName
      let clickEv = domEvent Click dirEl
      setRoute (FrontendRoute_TechDemo :/ [readerText] <$ clickEv)
    Left _e ->
      -- It was not.  Assume it is a normal file.  Just show the name.
      el "div" $ text entryName
  where
    readerText = T.decodeLatin1 entryReader
