{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RecursiveDo #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE LambdaCase #-}

module Pages.MagicFolders (magicFolders) where

import Control.Exception (Exception, SomeException)
import Control.Monad (void)
import qualified Data.Map.Strict as Map
import qualified Data.Text as T
import Reflex.Dom.Core
import Obelisk.Route.Frontend
import Common.Route
import Pages.TechDemo (doWormholeInvite)
import Pages.Widgets (errorBox, mdcButton, mdcDialog, mdcText)
import Control.Monad.IO.Class (MonadIO)
import Control.Monad.Fix (MonadFix)

import PerformEvent (performEventInThread)
import Static (logo)
import MagicFolder

-- | Represent the UI state related to handling folder invites.
data AcceptingInviteState
    = NotAccepting
    -- ^ The UI is not currently waiting for an invite code to be entered.
    | EnteringCode
    -- ^ The UI *is* currently waiting for an invite code to be entered.
    | ReceivingInvite
    -- ^ We've entered a code and await the conclusion of the invite.
    | ReceivingInviteFailed T.Text
    -- ^ We tried to process an invite code but encountered some problem.
    | InviteFinished
    -- ^ The invite has concluded successfully.
    deriving (Eq, Ord, Show)


-- | Show something for invite progress
indeterminateLinearProgress
    :: (DomBuilder t m, PostBuild t m) => Dynamic t AcceptingInviteState -> m ()
indeterminateLinearProgress acceptInviteState = do
  elDynAttr "div" progressClasses $ do
    elAttr "div" ("class" =: "mdc-linear-progress__buffer") $ do
      elAttr "div" ("class" =: "mdc-linear-progress__buffer-bar") blank
      elAttr "div" ("class" =: "mdc-linear-progress__buffer-dots") blank
    elAttr "div" ("class" =: "mdc-linear-progress__bar mdc-linear-progress__primary-bar") $ do
      elAttr "span" ("class" =: "mdc-linear-progress__bar-inner") blank
    elAttr "div" ("class" =: "mdc-linear-progress__bar mdc-linear-progress__secondary-bar") $ do
      elAttr "span" ("class" =: "mdc-linear-progress__bar-inner") blank
  where
    -- If you want the progress bar to always be visible, try something like this:
    -- progressClasses = inviteProgressAttributes 1 <$> constDyn 1
    progressClasses = inviteProgressAttributes ReceivingInvite <$> acceptInviteState


-- | Compute element attributes for the top-level element of an MDC
-- indeterminate linear progress indicator, given two values which decide its
-- visibility by their equality.
inviteProgressAttributes :: Eq a => a -> a -> Map.Map T.Text T.Text
inviteProgressAttributes when now'
  | when == now' = "class" =: commonClass <> commonParts
  | otherwise    = "class" =: (commonClass <> " mdc-linear-progress--closed") <> commonParts
    where
      commonClass = "mdc-linear-progress mdc-linear-progress--indeterminate"
      commonParts =
          "role" =: "progressbar"
          <> "aria-label" =: "Invite Progress"
          <> "aria-valuemin" =: "0"
          <> "aria-valuemax" =: "1"
          <> "aria-valuenow" =: "0"
          <> "data-mdc-auto-init" =: "MDCLinearProgress"

magicFolders
    :: forall t m . ( PerformEvent t m
       , DomBuilder t m
       , PostBuild t m
       , MonadHold t m
       , MonadIO (Performable m)
       , MonadFix m
       , TriggerEvent t m
       , SetRoute t (R FrontendRoute) m
       ) => Dynamic t (Either T.Text [MagicFolder]) -> (Either T.Text [MagicFolder] -> IO ()) -> m ()
magicFolders foldersDyn updateFolders = do
      rec
        elAttr "header" ("class" =: "mdc-top-app-bar mdc-top-app-bar--fixed" <> "data-mdc-auto-init" =: "MDCTopAppBar") $
          divClass "mdc-top-app-bar__row" $ do
            elClass "section" "mdc-top-app-bar__section mdc-top-app-bar__section--align-start" $ do
              (logotop, _) <- elAttr' "button" ("class" =: "material-icons mdc-top-app-bar__navigation-icon mdc-icon-button tahoe-logo-button") $
                elAttr "img" ("src" =: logo) blank
              elClass "span" "mdc-top-app-bar__title" $
                text "Tahoe-LAFS"

              -- an easter-egg to get to the tech-demo page
              -- (three clicks on the logo)
              counterDyn <- foldDyn (+) 0 (1 <$ domEvent Mousedown logotop)
              let easterEggEvent = ffilter (>= (3 :: Int)) (updated counterDyn)
              setRoute $ (FrontendRoute_TechDemo :/ []) <$ easterEggEvent

            elAttr "section" ("class" =: "mdc-top-app-bar__section mdc-top-app-bar__section--align-end" <> "role" =: "toolbar") blank


        (add, _) <- elClass' "main" "magic-folder-page mdc-top-app-bar--fixed-adjust" $ do
          indeterminateLinearProgress acceptInviteState

          elAttr "ul" ("class" =: "mdc-deprecated-list mdc-deprecated-list--avatar-list" <> "data-mdc-auto-init" =: "MDCList") $
              dyn_ $ either text (mapM_ folderWidget) <$> foldersDyn

          elAttr "button" ("class" =: "mdc-fab" <> "aria-label" =: "Add" <> "data-mdc-auto-init" =: "MDCRipple") $ do
            divClass "mdc-fab__ripple" blank
            elClass "span" "mdc-fab__icon material-icons" $
              text "add"

          -- Start out in the UI state where we're not waiting for any
          -- folder invite information. We progress through the other
          -- states in a circular fashion (EnteringCode,
          -- ReceivingInvite, InviteFinished then back to NotAccepting)

          -- transitions:
          --   click add -> EnteringCode
          --   form submitted -> ReceivingInvite
          --   (IO happened: folder-list updated, or...) -> InviteFinished
          --   dismiss clicked -> NotAccepting

        submitEitherInviteCode <- receiveFolderInviteText $ (EnteringCode ==) <$> acceptInviteState
        dismissError <- errorBox $ errorText <$> acceptInviteState
        let
          cancelInvite :: Event t ()
          submitInviteCode :: Event t T.Text
          (cancelInvite, submitInviteCode) = fanEither submitEitherInviteCode

        (inviteCompleted :: Event t (Either SomeException ())) <-
          performEventInThread $ ffor submitInviteCode $
            \code -> do
              void $ doWormholeInvite code
              loadMagicFolders >>= updateFolders

        let (inviteFailed, inviteSucceeded) = fanEither inviteCompleted

        let inviteEvents =
              [ EnteringCode <$ domEvent Click add
              , ReceivingInvite <$ submitInviteCode
              , stateForInviteFailure <$> inviteFailed
              , InviteFinished <$ inviteSucceeded
              , NotAccepting <$ dismissError
              , NotAccepting <$ cancelInvite
              ]

        acceptInviteState <- holdDyn NotAccepting (leftmost inviteEvents)

      el "script" $ do
          text "mdc.autoInit();"

-- | Create an invite state for a given exception.
--
-- Just shows the exception and wraps it in the error state value.
stateForInviteFailure :: Exception e => e -> AcceptingInviteState
stateForInviteFailure e = ReceivingInviteFailed (T.pack . show $ e)

-- | Extract the text of an error from the current invite state, if there is
-- any.
errorText :: AcceptingInviteState -> Maybe T.Text
errorText (ReceivingInviteFailed t) = Just t
errorText _ = Nothing

-- | A widget representing a single magic-folder.
folderWidget :: (DomBuilder t m, SetRoute t (R FrontendRoute) m) => MagicFolder -> m ()
folderWidget (MagicFolder name _state) = do
    (theEl', _) <- elClass' "li" "mdc-deprecated-list-item" $ do
      elClass "span" "mdc-deprecated-list-item__ripple" blank
      elAttr "span" ("class" =: "mdc-deprecated-list-item__graphic material-icons") $
        text "folder"
      elAttr "span" ("class" =: "mdc-deprecated-list-item__text") $ do
        text name
      -- Delete button.  TODO: Wiring.
      -- elAttr "span" ("class" =: "mdc-deprecated-list-item__meta") $
        -- elAttr "button" ("class" =: "mdc-icon-button") $ do
        --   divClass "mdc-icon-button__ripple" blank
        --   elAttr "i" ("class" =: "material-icons") $ do
        --     text "delete"

    elClass "li" "mdc-deprecated-list-divider mdc-deprecated-list-divider--inset" blank

    setRoute $ FrontendRoute_FileBrowser :/ (name, []) <$ domEvent Click theEl'


-- | An MDC-themed text input field.
inviteCode :: DomBuilder t m => m (InputElement EventResult (DomBuilderSpace m) t)
inviteCode = do
  elClass "span" "mdc-text-field__ripple" blank
  inputEl <- inviteCodeInput
  elClass "span" "mdc-floating-label" $ text "Enter Wormhole code"
  elClass "span" "mdc-line-ripple" blank
  pure inputEl


-- | A widget where a wormhole code can be submitted and when it is, the
-- currently active route is changed to the invite-handling route.
receiveFolderInviteText
    :: ( PerformEvent t m
       , DomBuilder t m
       , PostBuild t m
       )
    => Dynamic t Bool -> m (Event t (Either () T.Text))
receiveFolderInviteText visible = do
    -- A modal dialogue asking for a wormhole code.
      (_, inputEl, (closeButton, acceptButton)) <- mdcDialog visible (text "Add magic folder") (mdcText inviteCode) $ do
        (closeButton, _) <- mdcButton "close" $ do
          divClass "mdc-button__ripple" blank
          divClass "mdc-button__label" $ text "Cancel"
        (acceptButton, _) <- mdcButton "accept" $ do
          divClass "mdc-button__ripple" blank
          divClass "mdc-button__label" $ text "OK"
        pure (closeButton, acceptButton)

      let goAheadEv = leftmost [keypress Enter inputEl, void (domEvent Click acceptButton)]
          inviteCodeEv = tag (current (_inputElement_value inputEl)) goAheadEv
          inviteFinishedEv = leftmost [Left <$> domEvent Click closeButton, Right <$> inviteCodeEv]

      pure inviteFinishedEv

-- | Create a text input element for a magic-wormhole code suitable for use with an `MDCDialog`.
inviteCodeInput :: DomBuilder t m => m (InputElement EventResult (DomBuilderSpace m) t)
inviteCodeInput =
    inputElement inputElementConfig'
    where
      inputElementConfig = def
      inputElementConfig' = inputElementConfig {
        _inputElementConfig_elementConfig =
            (_inputElementConfig_elementConfig inputElementConfig)
            { _elementConfig_initialAttributes = "class" =: "mdc-text-field__input" <> "type" =: "text" <> "placeholder" =: "7-example-code"
            }
        }
