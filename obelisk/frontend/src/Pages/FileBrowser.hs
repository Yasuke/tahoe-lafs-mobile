{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE NamedFieldPuns #-}
{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE TupleSections #-}
{-# LANGUAGE RecursiveDo #-}

module Pages.FileBrowser (fileBrowser) where

import Data.Bifunctor (first)
import Data.Tuple.Extra (uncurry3)
import System.Directory (createDirectoryIfMissing)
import System.IO.Extra (newTempFileWithin)
import Data.List.Extra
import Reflex.Dom.Core
import Control.Exception (Exception, SomeException, throwIO)
import Tahoe.Announcement (Announcements(..))
import App (App(App, appGrid))
import PerformEvent (performEventInThread)
import Pages.Widgets (errorBox)
import System.Posix.Files (rename)
import FrontendPaths (getCacheDir, viewFile)
import qualified Data.ByteString as B
import qualified Data.ByteString.Lazy as LB
import Control.Monad.IO.Class (MonadIO, liftIO)
import Control.Monad.Fix (MonadFix)
import Tahoe.Download (DownloadError(..), download, announcementToImmutableStorageServer)
import qualified Data.Text as T
import Obelisk.Route.Frontend
import Common.Route
import MagicFolder
import Tahoe.MagicFoldr (FolderEntry(..))
import qualified Tahoe.CHK.Capability as CHK
import qualified Data.Map.Strict as Map

-- | Choose a Material Design 2 icon name to represent a given entry.
-- https://fonts.google.com/icons
iconForEntry :: FolderEntry -> T.Text
iconForEntry File { } = "insert_drive_file"
iconForEntry Directory { } = "folder_open"
iconForEntry Conflict { } = "dangerous"

-- | Create some simple textual information about a particular entry, for
-- inclusion in the file browser near that entry.
describeEntry :: FolderEntry -> T.Text
describeEntry File { fileContent } = humanReadableSize . CHK.size . CHK.verifier $ fileContent
describeEntry Directory { directoryChildren } =
    -- TODO What is the size of a directory?
    T.concat [ childCount , "; ", humanReadableSize 0 ]
        where
          directoryChildCount = Map.size directoryChildren
          childCount = case directoryChildCount of
                         1 -> "1 file"
                         n -> (T.pack . show $ n) <> " files"

describeEntry Conflict { conflictParticipants } =
  T.concat ("Conflict: ":intersperse ", " (T.pack . show <$> conflictParticipants))

humanReadableSize :: Integer -> T.Text
humanReadableSize n
    | n < 1000 = fmt 1 "B"
    | n < 1000000 = fmt 1000 "KB"
    | n < 1000000000 = fmt 1000000 "MB"
    | n < 1000000000000 = fmt 1000000000 "GB"
    | otherwise = fmt 1000000000000 "TB"
    where
      fmt :: Integer -> T.Text -> T.Text
      fmt m suffix = (T.pack . show . (`div` m) $ n) <> " " <> suffix

data LeaveAction
    = Close -- ^ Close the file browser.
    | Up    -- ^ Navigate up one level in the directory hierarchy.
      deriving (Eq, Ord, Show)

-- | For a given file browser path, determine what the "leave this page"
-- action means.
leaveAction :: [T.Text] -> LeaveAction
leaveAction [] = Close
leaveAction _ = Up

leaveActionIcon :: LeaveAction -> T.Text
leaveActionIcon Close = "close"
leaveActionIcon Up = "arrow_back"

-- | Define the entire view for looking at a single folder and its direct
-- contents.
fileBrowser :: ( DomBuilder t m
               , PerformEvent t m
               , MonadIO (Performable m)
               , PostBuild t m
               , MonadHold t m
               , MonadFix m
               , SetRoute t (R FrontendRoute) m
               , Routed t FolderLocation m
               , TriggerEvent t m
               ) => App t -> [(EntryName, FolderEntry)] -> m ()
fileBrowser App{appGrid} entries = do
      routeDyn <- askRoute
      elAttr "header" ("class" =: "mdc-top-app-bar mdc-top-app-bar--fixed" <> "data-mdc-auto-init" =: "MDCTopAppBar") $
        divClass "mdc-top-app-bar__row" $ do
          elClass "section" "mdc-top-app-bar__section mdc-top-app-bar__section--align-start" $ do
            (leaveButton, _) <- elAttr' "button" ("class" =: "material-icons mdc-top-app-bar__navigation-icon mdc-icon-button" <> "aria-label" =: "Close") $
              dynText $ leaveActionIcon . leaveAction . snd <$> routeDyn
            elClass "span" "mdc-top-app-bar__title" $
              text "File browser" -- TODO: Show current folder name here
            setRoute $ ffor (tag (current routeDyn) (domEvent Click leaveButton)) $ \case
                  (_, []) -> FrontendRoute_MagicFolders :/ ()
                  (folder, path) -> FrontendRoute_FileBrowser :/ (folder, dropEnd1 path)

          -- Action buttons on the FileBrowser top app bar work like this:
          -- elAttr "section" ("class" =: "mdc-top-app-bar__section mdc-top-app-bar__section--align-end" <> "role" =: "toolbar") $ do
          --   elAttr "button" ("class" =: "mdc-icon-button material-icons mdc-top-app-bar__action-item" <> "aria-label" =: "Share") $
          --     text "share"
          --   elAttr "button" ("class" =: "mdc-icon-button material-icons mdc-top-app-bar__action-item" <> "aria-label" =: "Delete") $
          --     text "delete"
          --   elAttr "button" ("class" =: "mdc-icon-button material-icons mdc-top-app-bar__action-item" <> "aria-label" =: "Open menu") $
          --     text "more_vert"


      -- Collect up the download result events for every entry
      downloadResults <- elClass "main" "mdc-top-app-bar--fixed-adjust" $

        elAttr "ul" ("class" =: "mdc-deprecated-list mdc-deprecated-list--two-line mdc-deprecated-list--avatar-list" <> "data-mdc-auto-init" =: "MDCList") $
            mapM (uncurry $ fileBrowserEntryWidget appGrid) entries

      -- Narrow to a download failure event for any entry
      let (downloadFailedEv, _) = fanEither (leftmost downloadResults)

      rec
        -- Construct a Dynamic with no error (initially and after an error is
        -- dismissed) or the most recent error.
        currentErrorDyn <- holdDyn Nothing $ leftmost [Just <$> downloadFailedEv, Nothing <$ dismissEv]

        -- Create an error dialog that shows the error if there is one and tells
        -- us when it has been dismissed.
        dismissEv <- errorBox currentErrorDyn

      el "script" $
          text "mdc.autoInit();"


-- | Define the view for a single item in a file browser, including an icon,
-- the item name, and some metadata.
fileBrowserEntryWidget
    :: forall t m.
    ( DomBuilder t m
    , PerformEvent t m
    , MonadIO (Performable m)
    , SetRoute t (R FrontendRoute) m
    , Routed t FolderLocation m
    , TriggerEvent t m
    )
    => Dynamic t (Either T.Text Announcements)
    -- ^ Server configuration for downloads
    -> EntryName
    -- ^ The name of the entry in this widget.
    -> FolderEntry
    -- ^ Additional details about the entry in this widget.
    -> m (Event t (Either T.Text ()))
    -- ^ An event that triggers if this entry is downloaded, after the
    -- download fails or succeeds.
fileBrowserEntryWidget gridDyn (EntryName name) entry = do
    (theEl, _) <- elClass' "li" "mdc-deprecated-list-item" $ do
      elClass "span" "mdc-deprecated-list-item__ripple" blank
      elAttr "span" ("class" =: "mdc-deprecated-list-item__graphic material-icons") $
        text $ iconForEntry entry
      elAttr "span" ("class" =: "mdc-deprecated-list-item__text") $ do
        elAttr "span" ("class" =: "mdc-deprecated-list-item__primary-text") $
          text name
        elAttr "span" ("class" =: "mdc-deprecated-list-item__secondary-text") $
          text $ describeEntry entry

    elClass "li" "mdc-deprecated-list-divider mdc-deprecated-list-divider--inset" blank

    case entry of
      Directory { } -> do
        r <- askRoute
        let navigateEv = tag (current r) (domEvent Click theEl)
        setRoute $ ffor navigateEv $
            \(folder, path) -> FrontendRoute_FileBrowser :/ (folder, Data.List.Extra.snoc path name)
        pure never

      File { fileContent } -> do
          downloadComplete <- performEventInThread $
            ffor ((,name,fileContent) <$> canDownloadEv) $
            liftIO . uncurry3 viewContent
          pure $ first (T.pack . show) <$> (downloadComplete :: Event t (Either SomeException ()))

          where
            downloadEv = tag (current gridDyn) (domEvent Click theEl)
            -- XXX If we don't have a grid right now we can't even try to
            -- download.  It would be great if something took care of this
            -- before we got here.  For the moment, we do nothing with this
            -- information - we just silently don't act.
            (_cantDownloadEv, canDownloadEv) = fanEither downloadEv

      _ -> pure never

data DownloadErrorWrapper
    = DownloadErrorWrapper DownloadError
    | CacheDirectoryConfigurationMissing
    deriving (Show)
instance Exception DownloadErrorWrapper

-- | Use the given capability to download some content and attempt to display
-- it to the user of the device.
viewContent :: Announcements -> T.Text -> CHK.Reader -> IO ()
viewContent (Announcements grid) name fileContent = do
  p <- getCacheDir
  case p of
    Nothing -> throwIO CacheDirectoryConfigurationMissing
    Just cacheDir -> do
      result <- download grid fileContent announcementToImmutableStorageServer
      case result of
        Left err -> throwIO (DownloadErrorWrapper err)
        Right content -> do
          createDirectoryIfMissing True downloadDirectory
          (tempName, _) <- newTempFileWithin downloadDirectory
          -- XXX Ideally we could do _some_ laziness here.
          B.writeFile tempName (LB.toStrict content)
          rename tempName finalName
          viewFile finalName
          where
            finalName = downloadDirectory <> T.unpack name

            -- For this to work on Android, the directory must referenced by a
            -- correct declaration in the Android manifest xml file.
            downloadDirectory = cacheDir <> "/downloads/"
