{-# LANGUAGE GADTs #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE ScopedTypeVariables #-}

module Navigation where

import JSDOM (currentWindow)
import JSDOM.History (back)
import JSDOM.Window (getHistory)
import JSDOM.Types (MonadDOM)
import Control.Monad.IO.Class (liftIO)

import PlatformEvents (PlatformEvent(..))

-- | Handle platform events.
platformEventCallback :: MonadDOM m => PlatformEvent -> m ()
platformEventCallback BackPressed = navigateBack

-- | Perform a standard "back" navigation through route history.
navigateBack :: MonadDOM m => m ()
navigateBack = do
    windowM <- currentWindow
    case windowM of
      -- No window, no history navigation...
      Nothing -> liftIO $ do
        print "Back button but no window ..."
      Just window -> do
        history <- getHistory window
        back history
