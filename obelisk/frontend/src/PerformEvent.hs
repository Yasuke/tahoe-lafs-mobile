{-# LANGUAGE FlexibleContexts #-}

module PerformEvent where

import Control.Exception (Exception, try)
import Control.Concurrent (forkIO)
import Control.Monad (void)
import Control.Monad.IO.Class (MonadIO, liftIO)
import Reflex.Dom.Core
    ( TriggerEvent
    , PerformEvent
    , Performable
    , Event
    , performEventAsync
    , ffor
    )

-- | Execute IO actions from an event in a separate thread and produce an
-- Event that triggers with their results.  Ordering is not guaranteed.
performEventInThread
    :: ( TriggerEvent t m
       , PerformEvent t m
       , MonadIO (Performable m)
       , Exception e
       )
    => Event t (IO a)
    -> m (Event t (Either e a))
performEventInThread evt =
  performEventAsync $ ffor evt $
    \action callback -> liftIO . void . forkIO $ try action >>= callback
