{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TemplateHaskell #-}

-- | Define the locations of static content served up separately from the
-- frontend page.  Since these locations are computed using TemplateHaskell we
-- isolate them here to avoid needing to apply TemplateHaskell to the rest of
-- the implementation modules.
module Static where

import qualified Data.Text as T

import Obelisk.Generated.Static (static)

-- | The location of the app's main stylesheet.
mainCss :: T.Text
mainCss = $(static "main.css")

-- | The location of one of the logos for the app.
logoWithText :: T.Text
logoWithText = $(static "tahoe-logo-with-text.png")

-- | The location of one of the logos for the app.
logo :: T.Text
logo = $(static "tahoe-logo-icon.png")

-- | The location of the Material Design Components (MDC) CSS.
materialComponentsCss :: T.Text
materialComponentsCss = $(static "3rdparty/material-components-web.min.css")

-- | The location of the Material Design Components (MDC) JS.
materialComponentsJs :: T.Text
materialComponentsJs = $(static "3rdparty/material-components-web.min.js")

-- | The location of the Material Design Icons.
materialIconsCss :: T.Text
materialIconsCss = $(static "3rdparty/material-icons.css")

-- | The location of the Roboto font.
robotoCss :: T.Text
robotoCss = $(static "3rdparty/roboto/roboto.css")
