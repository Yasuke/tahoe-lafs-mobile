{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE NamedFieldPuns #-}
{-# LANGUAGE ConstraintKinds #-}

module Controller where

import Data.Bifunctor (first)
import qualified Data.Map.Strict as Map
import Control.Monad.IO.Class (MonadIO, liftIO)
import Control.Monad.Fix (MonadFix)
import Control.Exception (SomeException, try)
import qualified Data.Text as T
import Reflex.Dom.Core
import Obelisk.Route (R)
import Obelisk.Route.Frontend
import Pages.FirstRun (firstRunPage)
import Pages.TechDemo (techDemoPage)
import Pages.FileBrowser (fileBrowser)
import Pages.MagicFolders (magicFolders)
import Common.Route (FrontendRoute(..), FolderLocation)
import MagicFolder (EntryName(..), MagicFolder(..))
import Tahoe.MagicFoldr (FolderEntry(..))
import App (App(..))

-- dyn :: (Adjustable t m, NotReady t m, PostBuild t m) => Dynamic t (m a) -> m (Event t a)
-- constDyn :: Reflex t => a -> Dynamic t a
-- holdDyn :: a -> Event t a -> m (Dynamic t a)
-- switch :: Behavior t (Event t a) -> Event t a
-- current :: Dynamic t a -> Behavior t a

-- widgetHold :: (Adjustable t m, MonadHold t m) => m a -> Event t (m a) -> m (Dynamic t a)
-- switchHold ??

-- el' :: forall t m a. DomBuilder t m => Text -> m a -> m (Element EventResult (DomBuilderSpace m) t, a)
-- button :: DomBuilder t m => Text -> m (Event t ())


-- | Choose an initial page to show while the app has initialized itself.
data FirstPage
    = Loading
    -- ^ We haven't loaded enough application state from the filesystem to
    -- know what page to show yet.
    | FirstRun
    -- ^ The application state includes no magic-folders so show the
    -- first-time welcome screen.
    | BrowseFolders
    -- ^ There are some folders so jump straight to the magic-folder browser.
    deriving (Eq)

-- | Given current application initialization state, choose a page to show.
whichPage :: Bool -> Either T.Text [MagicFolder] -> FirstPage
whichPage False _ = Loading
whichPage True (Right []) = FirstRun
whichPage True (Right (_:_)) = BrowseFolders
whichPage True _ = FirstRun

-- | Show the chosen page.
renderFirstPage :: (SetRoute t (R FrontendRoute) m, DomBuilder t m) => FirstPage -> m ()
renderFirstPage Loading = blank
renderFirstPage FirstRun = firstRunPage
renderFirstPage BrowseFolders = blank

-- | Show the first run page if we have no magic-folders, otherwise navigate
-- to the magic-folder browser.
maybeFirstRun ::
    ( PerformEvent t m
    , DomBuilder t m
    , SetRoute t (R FrontendRoute) m
    , PostBuild t m
    )
    => App t
    -> RoutedT t a m ()
maybeFirstRun app = do
  let pageDyn = fmap (uncurry whichPage) (appInitialized app)
  let browseEvent = (FrontendRoute_MagicFolders :/ ()) <$ ffilter (== BrowseFolders) (updated pageDyn)
  setRoute browseEvent
  dyn_ $ fmap renderFirstPage pageDyn

pageFromRoute ::
    ( PerformEvent t m
    , MonadIO (Performable m)
    , DomBuilder t m
    , SetRoute t (R FrontendRoute) m
    , SetRoute t (R FrontendRoute) (Client m)
    , PostBuild t m
    , Prerender t m
    , MonadHold t m
    , MonadFix m
    , TriggerEvent t m
    )
    => App t
    -> FrontendRoute a
    -> RoutedT t a m ()

pageFromRoute app FrontendRoute_Main = maybeFirstRun app

pageFromRoute App{appGrid, appFolders, appUpdateFoldersList} FrontendRoute_TechDemo = do
  r <- askRoute
  techDemoPage appGrid appFolders appUpdateFoldersList r

pageFromRoute App{appFolders, appUpdateFoldersList} FrontendRoute_MagicFolders = magicFolders appFolders appUpdateFoldersList

pageFromRoute app@App{appFolders} FrontendRoute_FileBrowser = do
  r <- askRoute
  dyn_ $ uncurry (fileBrowserIfYouCan app) <$> r <*> appFolders

type FileBrowser t m =
  ( MonadHold t m
  , MonadIO (Performable m)
  , PerformEvent t m
  , TriggerEvent t m
  , PostBuild t m
  , DomBuilder t m
  , MonadFix m
  , SetRoute t (R FrontendRoute) m
  , Routed t FolderLocation m
  )

-- | Try to make a file browser widget.
fileBrowserIfYouCan
    :: FileBrowser t m
    => App t
    -- ^ Global application state!
    -> T.Text
    -- ^ The _name_ of the magic-folder to browse.
    -> [T.Text]
    -- ^ The path to the directory in the folder to browse.
    -> Either T.Text [MagicFolder]
    -- ^ The magic-folders we know about.
    -> m ()
fileBrowserIfYouCan _ _ _ (Left err)  = text err
fileBrowserIfYouCan _ _ _ (Right [])  = text "Folder missing"
fileBrowserIfYouCan app@App{appFolderContents} requestedFolderName path (Right (folder:fs))
    | requestedFolderName == magicFolderName folder =
      dyn_ $ fileBrowser' app folder path  . Map.findWithDefault defaultFolderEntry folder <$> appFolderContents

    | otherwise = fileBrowserIfYouCan app requestedFolderName path (Right fs)

-- | Make a file browser widget for a certain magic-folder, possibly visiting
-- some subdirectory.
fileBrowser'
    :: FileBrowser t m
    => App t
    -- ^ Global application state!
    -> MagicFolder
    -- ^ The particular magic-folder to browse.
    -> [T.Text]
    -- ^ The path to the directory in the folder to browse.
    -> FolderEntry
    -- ^ The root of the magic-folder to browser.
    -> m ()
fileBrowser' app@App{appUpdateFolder} folder path rootEntry  = do
  goEv <- getPostBuild
  -- It might be a good idea to replace this performEvent_ with a
  -- performEventInThread.  However, the action being performed is only a
  -- Reflex trigger-an-Event.  I suppose that this operation is relatively
  -- fast and relatively safe?  I will stick some ad hoc error handling inside
  -- so if something surprising happens at least the app doesn't enter a
  -- broken state.
  performEvent_ $ ffor goEv $ \() -> liftIO $ do
    result <- try $ appUpdateFolder folder
    case result of
      Left e -> print (e :: SomeException)
      Right _ -> pure ()

  fileBrowser app (listWhichDir path rootEntry)

-- | List the contents of a directory arbitrarily deeply nested beneath some
-- root.
listWhichDir
    :: [T.Text]
    -- ^ The path segments identifying the directory to list.
    -> FolderEntry
    -- ^ The root directory from which to start.
    -> [(EntryName, FolderEntry)]
    -- ^ The direct children of the selected directory.
listWhichDir [] rootEntry@(Directory _) = listThisDir rootEntry
listWhichDir (next:rest) (Directory dc) = concatMap (listWhichDir rest) (Map.lookup next dc)
-- Just rely on listThisDir to produce an error for this case.
listWhichDir _ entry = listThisDir entry

-- | Given a Directory, return the direct children annotated with their
-- basenames.
-- XXX Move this into some failable context.
listThisDir :: FolderEntry -> [(EntryName, FolderEntry)]
listThisDir (Directory dc) =  first EntryName <$> Map.assocs dc
listThisDir _ = [(EntryName "the system has failed, sorry, please file a bug report", defaultFolderEntry)]

-- | An empty directory.
defaultFolderEntry :: FolderEntry
defaultFolderEntry = Directory mempty
