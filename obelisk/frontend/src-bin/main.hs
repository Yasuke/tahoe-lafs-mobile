import Obelisk.Frontend (runFrontend)
import Obelisk.Route.Frontend (checkEncoder)
import Data.IORef (IORef, newIORef, readIORef)

import Frontend (makeFrontend)
import Common.Route (fullRouteEncoder)
import FrontendPaths (runWithRef)

main :: IO ()
main = do
  let Right validFullEncoder = checkEncoder fullRouteEncoder
  -- Run the frontend with a shared reference to a structure of
  -- platform-event-related callbacks.
  runWithRef (runFrontend validFullEncoder . makeFrontend)
