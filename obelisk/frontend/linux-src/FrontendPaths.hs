-- | Implement platform-specific path-related operations for the Linux
-- platform.
module FrontendPaths (getFilesDir, getCacheDir, runWithRef, viewFile) where

import Data.IORef (IORef, newIORef)
import System.Environment.XDG.BaseDir (getUserCacheDir, getUserDataDir)
import System.Directory (createDirectoryIfMissing)
import Reflex.Dom (run)
import PlatformEvents (PlatformEventCallbacks)
import Language.Javascript.JSaddle (JSM)
import Data.Default.Class (def)

-- | Get a dedicated directory for this application beneath the XDG standard
-- cache directory.
getCacheDir :: IO (Maybe FilePath)
getCacheDir = do
  dataPath <- getUserCacheDir "tahoelafsmobile"

  createDirectoryIfMissing True dataPath
  pure $ Just dataPath

-- | Get the path to a directory that is specific to the application where
-- internal application data may be written (and read back).
getFilesDir :: IO (Maybe FilePath)
getFilesDir = do
  dataPath <- getUserDataDir "tahoelafsmobile"

  createDirectoryIfMissing True dataPath
  pure $ Just dataPath

-- | Run a frontend on Linux (not `ob run`).
runWithRef :: (Maybe (IORef PlatformEventCallbacks) -> JSM ()) -> IO ()
runWithRef makeJSM =
  -- Off Android, we have no platform integration or event handlers so just
  -- supply a no-op callback to fill the slot and then proceed.
  newIORef def >>= run . makeJSM . Just

-- | Show a file to the user.
viewFile :: FilePath -> IO ()
viewFile p = print $ "I viewed " <> p
