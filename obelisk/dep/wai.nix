{ mkDerivation, base, bytestring, hspec, hspec-discover, http-types, network, text, vault, lib
}:
mkDerivation {
  pname = "wai";
  version = "3.2.3";
  sha256 = "1y19h9v0cq1fl17ywcyyvd6419fhgyw2s0yk0ki8z60021adcx2m";
  libraryHaskellDepends = [
    base bytestring http-types network text vault
  ];
  testHaskellDepends = [ base bytestring hspec ];
  testToolDepends = [ hspec-discover ];
  description = "Web Application Interface";
  license = lib.licenses.mit;
}
