# Use one of Obelisk/Reflex's shells.  This at least gets us a working ghcid
# and haskell-language-server, extremely useful for development purposes.
(import ./default.nix { }).shells.ghc
