{nix-thunk}: self: super: rec {
  # The build of cross-compiled libsodium fails because separateDebugInfo doesn't output
  # anything - A fix is available from nixpkgs 23.11 or so, see
  # https://github.com/NixOS/nixpkgs/commit/94c7bf576a44829a676fb172bfceaefff4eb90f0
  # For now, just turn separateDebugInfo off.
  #libsodium = super.libsodium.overrideAttrs (old: { dontStrip = true; separateDebugInfo = false; });
  # We require libsodium >= 1.0.19 for its portability fixes
  libsodium = (self.callPackage ./dep/libsodium.nix { }).overrideAttrs (old: { dontStrip = true; separateDebugInfo = false; });

  # On MacOS, openldap tests fail.  We don't care. (openldap > apr-util > subversion > xcode)
  openldap = super.openldap.overrideAttrs (old: { doCheck = false; });
  apr-util = super.apr-util.override (old: { ldapSupport = false; });
}
